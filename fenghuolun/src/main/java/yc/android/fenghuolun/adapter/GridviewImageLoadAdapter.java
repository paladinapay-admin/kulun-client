package yc.android.fenghuolun.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import yc.android.fenghuolun.R;
import yc.android.fenghuolun.utils.ImageLoader;

//Adapter class extends with BaseAdapter and implements with OnClickListener
public class GridviewImageLoadAdapter extends BaseAdapter implements OnClickListener{
    private Activity activity;
    private ArrayList<String> data = new ArrayList<String>();
    private static LayoutInflater inflater=null;
    private ImageLoader imageLoader;
    private HashMap<Integer,ImageView> imagesHash = new HashMap<Integer, ImageView>();
//    private int endPosition = 8;


    private int category_position;

//    public GridviewImageLoadAdapter(Activity a, ArrayList<String> d, int c_position) {
//        activity = a;
//        data = d;
//        category_position = c_position;
//
//
//        inflater = (LayoutInflater)activity.
//                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        // Create ImageLoader object to download and show image in list
//        // Call ImageLoader constructor to initialize FileCache
//        imageLoader = new ImageLoader(activity.getApplicationContext());
//
//    }

    public GridviewImageLoadAdapter(Activity a, int c_position) {
        activity = a;
//        data = d;
        category_position = c_position;


        inflater = (LayoutInflater)activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // Create ImageLoader object to download and show image in list
        // Call ImageLoader constructor to initialize FileCache
        imageLoader = new ImageLoader(activity.getApplicationContext());

    }

    public void flush_previous_data(){
        data.clear();;
    }

    public void setCurrentData(ArrayList<String> d){
        for (int i = 0; i < d.size() ; i ++){
            String temp_d = d.get(i);
            if (!data.contains(temp_d)) {
                data.add(temp_d);
            }
        }
    }

//    public void setEndPosition(int newEndPosition){
//        endPosition = newEndPosition;
//    }
//
//    public int getEndPosition(){
//        return endPosition;
//    }

    public ImageLoader getImageLoader(){
        return imageLoader;
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder {
        public ImageView image;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;
        String gridview_item_index = "gridview_item_" + Integer.toString(category_position ) ;

        if(convertView==null) {
            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/

            int layout_item = activity.getResources().getIdentifier(gridview_item_index, "layout",
                    activity.getApplicationContext().getPackageName());

            vi = inflater.inflate(layout_item, null);
//            vi = inflater.inflate(R.layout.gridview_item, null);
//            vi = inflater.inflate(Integer.parseInt(gridview_item_index), null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new ViewHolder();
            holder.image = (ImageView) vi.findViewById(R.id.img_display);

            /************  Set holder with LayoutInflater ************/
            vi.setTag(holder);
        }
        else
            holder=(ViewHolder)vi.getTag();

//        //TODO try load first 8 then second 8 images
//        while (position < endPosition && position < data.size()) {
            ImageView image = holder.image;

            //DisplayImage function from ImageLoader Class


            imageLoader.DisplayImage(data.get(position), image);
            imagesHash.put(position, image);
//        }
//        category_position_imagesHash.put(category_position, imagesHash);
//        bitmap = imageLoader.DisplayImage(data[position], image);
        /******** Set Item Click Listner for LayoutInflater for each row ***********/
        vi.setOnClickListener(new OnItemClickListener(position));
        return vi;
    }

    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub

    }

    /********* Called when Item click in Gridview ************/
    private class OnItemClickListener implements OnClickListener{
        private int mPosition;
        OnItemClickListener(int position){
            mPosition = position;
        }

        @Override
        public void onClick(View arg0) {
            Bitmap bp = ((BitmapDrawable)imagesHash.get(mPosition).getDrawable()).getBitmap();


//            download(data.get(mPosition), (Bitmap) category_position_bitmaps.get(category_position).get(mPosition),category_position);
            download(data.get(mPosition), bp,category_position);


        }
    }

//    public void onClick(int mPosition, Bitmap bitmap)
//    {
//        String tempValues = mStrings[mPosition];
//        Toast.makeText(activity, "Image URL : " + tempValues, Toast.LENGTH_LONG).show();
//        download(tempValues, bitmap);
//    }

    public void download(final String currentUrl, final Bitmap bitmapfordownload, final int dir_category_index){
//        final Bitmap bp = bitmap;
//        grid.geti

//        Toast.makeText(activity,"Image URL : "+currentUrl,Toast.LENGTH_LONG).show();
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        storeImageToExternalStorage(bitmapfordownload,currentUrl,dir_category_index);
                        Toast.makeText(activity, "Yes Clicked",
                                Toast.LENGTH_LONG).show();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        Toast.makeText(activity, "No Clicked",
                                Toast.LENGTH_LONG).show();
                        break;
                }
            }

//            AlertDialog.Builder builder = new AlertDialog.Builder(TryDynamicListActivity.this);



//            .setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();

        };
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage("确认购买？")
                .setPositiveButton("确认",dialogClickListener)
                .setNegativeButton("取消",dialogClickListener).show();
    }

    private void storeImageToExternalStorage(Bitmap bitmap, String url, int category_index) {


        String imagename =  url.substring(url.lastIndexOf('/')+1);


        if (true) {
            //Later need to be implemented as a purchasing function

            String pdir = activity.getFilesDir().getPath();
            File SDCardRoot = new File(pdir+"/Gallery/Category" + Integer.toString(category_index)+"/");
//            Toast.makeText(activity,"file dirs are"+SDCardRoot,Toast.LENGTH_LONG).show();
//            String filename = "image1.png";
            File file = new File(SDCardRoot, imagename);

            try {
                FileOutputStream fileOutput = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, fileOutput);
                fileOutput.flush();
                fileOutput.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

}