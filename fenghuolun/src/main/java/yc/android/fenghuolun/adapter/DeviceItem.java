package yc.android.fenghuolun.adapter;

import android.graphics.drawable.Drawable;

/**
 * Created by Yichang on 2014/6/16.
 */
public class DeviceItem {
    private Drawable icon;

    private String deviceName;

    private String deviceDescription;

    public String getDeviceName() {
        return this.deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getDeviceDescription() {
        return this.deviceDescription;
    }

    public void setDeviceDescription(String deviceDescription) {
        this.deviceDescription = deviceDescription;
    }
}
