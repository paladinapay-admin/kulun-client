package yc.android.fenghuolun.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import yc.android.fenghuolun.R;

/**
 * Created by sasha on 19/08/14.
 */
public class CheckboxListAdapter extends BaseAdapter implements View.OnClickListener{
    private static final String TAG = "fenghuolun";

    //private LayoutInflater inflater;
    private ArrayList<XHLPrograms> dataList;
    private Context context;
    private ArrayList<Integer> selectedPrograms;

    public CheckboxListAdapter(Context c, ArrayList<XHLPrograms> programs){
        super();
        //this.inflater = inflater;

        dataList = programs;
        context = c;
        selectedPrograms = new ArrayList<Integer>();

    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup){
        Log.d(TAG,"in CheckboxListAdapter getView()");
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.individual_program, viewGroup, false);

        XHLPrograms temp = dataList.get(position);

        TextView programName = (TextView) row.findViewById(R.id.program_name);
        TextView numOfPics = (TextView) row.findViewById(R.id.num_of_pics);
        CheckBox cb = (CheckBox) row.findViewById(R.id.checkBox);
        cb.setTag(temp);

        row.findViewById(R.id.checkBox).setOnClickListener(this);


        programName.setText(temp.getName());
        numOfPics.setText("含图(张)：" + temp.getNumOfPics());

        if(temp.getIsPlaying()==true) {
            programName.setTypeface(null, Typeface.BOLD);
            numOfPics.setTypeface(null, Typeface.BOLD);
        }


        return row;
    }

    @Override
    public Object getItem(int position){
        Log.d(TAG,"in CheckboxListAdapter getItem()");
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position){
        Log.d(TAG,"in CheckboxListAdapter getItemId()");
        return position;
    }

    @Override
    public int getCount(){
        Log.d(TAG,"in CheckboxListAdapter getCount()");
        return dataList.size();
    }

    @Override
    public void onClick(View view){
        Log.e(TAG,"in onClick");

        XHLPrograms xhlProgram = (XHLPrograms) view.getTag();
        xhlProgram.setToBeDeleted(((CheckBox) view).isChecked());
        Log.e(TAG,xhlProgram.getName() +" listId: " + xhlProgram.getListId() + " to be deleted?");
        //Toast.makeText(context.getApplicationContext(),xhlProgram.getName() +" id: " + xhlProgram.getListId() + " to be deleted?"+xhlProgram.getToBeDeleted(), Toast.LENGTH_SHORT).show();
        if (xhlProgram.getToBeDeleted()==true) {
            selectedPrograms.add(xhlProgram.getListId());
        }
        else{
            selectedPrograms.remove(selectedPrograms.indexOf(xhlProgram.getListId()));
        }

    }


    public ArrayList<Integer> getSelectedPrograms(){
        return selectedPrograms;

    }

    /*
    public void clear(){
        dataList.clear();
    }

    public void add(String item){
        Log.d(TAG,"in adding program item" );
        dataList.add(item);

    }*/

}
