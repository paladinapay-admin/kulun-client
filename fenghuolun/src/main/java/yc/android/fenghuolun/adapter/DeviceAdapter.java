package yc.android.fenghuolun.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fortysevendeg.swipelistview.SwipeListView;

import java.util.List;

import yc.android.fenghuolun.R;
import yc.android.fenghuolun.activity.ConfigActivity;
import yc.android.fenghuolun.activity.DeviceListActivity;
import yc.android.fenghuolun.activity.MainActivity;

/**
 * Created by Yichang on 2014/6/16.
 */
public class DeviceAdapter extends BaseAdapter {


    private List<DeviceItem> devices;
    private Context context;
    private SwipeListView swipeListView;

    public DeviceAdapter(Context context, List<DeviceItem> devices, SwipeListView swipeListView) {
        this.context = context;
        this.devices = devices;
        this.swipeListView = swipeListView;
    }

    @Override
    public int getCount() {
        return devices.size();
    }

    @Override
    public DeviceItem getItem(int position) {
        return devices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final DeviceItem item = getItem(position);
        final int p = position;
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.device_item_layout, parent, false);
            holder = new ViewHolder();
            holder.deviceImage = (ImageView) convertView.findViewById(R.id.img_device);
            holder.deviceName = (TextView) convertView.findViewById(R.id.txt_device_name);
            holder.deviceDescription = (TextView) convertView.findViewById(R.id.txt_device_description);
            holder.btn_setting = (Button) convertView.findViewById(R.id.btn_setting);
            //holder.btn_del = (Button) convertView.findViewById(R.id.btn_del);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ((SwipeListView) parent).recycle(convertView, position);

        holder.deviceImage.setImageDrawable(item.getIcon());
        holder.deviceName.setText(item.getDeviceName());
        holder.deviceDescription.setText(item.getDeviceDescription());

        holder.btn_setting.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return swipeListView.getTouchListener().isPaused();
            }
        });

        holder.btn_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = context.getPackageManager().getLaunchIntentForPackage(item.getDeviceName());
//                if (intent != null) {
//                    context.startActivity(intent);
//                } else {
//                    Toast.makeText(context, R.string.cantOpen, Toast.LENGTH_SHORT).show();
//                }
                Toast.makeText(context, "btn_setting "+ p+" is clicked", Toast.LENGTH_SHORT).show();
                ((MainActivity)context).configDevice(p);
                swipeListView.closeOpenedItems();
            }
        });

        /*
        holder.btn_del.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return swipeListView.getTouchListener().isPaused();
            }
        });

        holder.btn_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (isPlayStoreInstalled()) {
//                    context.startActivity(new Intent(Intent.ACTION_VIEW,
//                            Uri.parse("market://details?id=" + item.getPackageName())));
//                } else {
//                    context.startActivity(new Intent(Intent.ACTION_VIEW,
//                            Uri.parse("http://play.google.com/store/apps/details?id=" + item.getPackageName())));
//                }
                Toast.makeText(context, "btn_del is clicked", Toast.LENGTH_SHORT).show();
            }
        });
        */

//        holder.bAction3.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Uri packageUri = Uri.parse("package:" + item.getPackageName());
//                Intent uninstallIntent;
//                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
//                    uninstallIntent = new Intent(Intent.ACTION_DELETE, packageUri);
//                } else {
//                    uninstallIntent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE, packageUri);
//                }
//                context.startActivity(uninstallIntent);
//            }
//        });




        return convertView;
    }

    static class ViewHolder {
        ImageView deviceImage;
        TextView deviceName;
        TextView deviceDescription;
        Button btn_setting;
        //Button btn_del;
    }

}
