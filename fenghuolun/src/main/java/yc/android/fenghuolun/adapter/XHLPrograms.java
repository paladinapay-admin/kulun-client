package yc.android.fenghuolun.adapter;

/**
 * Created by sasha on 25/08/14.
 */
public class XHLPrograms {
    private String name;
    private int listId;
    private int programId;
    private int numOfPics;
    private int timePerPic;
    private int repeat;
    private int numOfSections;
    private boolean toBeDeleted;
    private boolean isPlaying;

    public XHLPrograms(String name, int listId, int numOfPics, boolean toBeDeleted, boolean isPlaying){
        this.name = name;
        this.listId = listId;
        this.numOfPics = numOfPics;
        this.toBeDeleted = toBeDeleted;
        this.isPlaying = isPlaying;
    }

    public String getName(){
        return name;
    }

    public int getListId() { return listId; }

    public int getNumOfPics(){
        return numOfPics;
    }

    public void setToBeDeleted(boolean delete){
        toBeDeleted = delete;
    }

    public boolean getToBeDeleted(){
        return toBeDeleted;
    }

    public void setIsPlaying(boolean isPlaying){ this.isPlaying = isPlaying; }

    public boolean getIsPlaying(){ return isPlaying; }
}
