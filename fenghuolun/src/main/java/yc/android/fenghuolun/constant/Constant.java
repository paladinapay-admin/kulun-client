package yc.android.fenghuolun.constant;

import android.content.res.Resources;

import com.loopj.android.http.AsyncHttpClient;

/**
 * Created by yichangzhang on 6/10/2014.
 */
public class Constant {

    public static final int interval_between_two_get_code_requests = 60000; //unit ms
    public static final int tick_interval = 1000; //unit ms

    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

//    public static final String host_url = "http://192.168.1.103:8000";
    //public static final String host_url = "http://182.254.149.159";

//    public static final String host_url = "http://192.168.1.104:8000";
public static final String host_url = "http://192.168.43.26:8000";
//    public static final String host_url = "http://192.168.1.27:8000";
//    public static final String host_url = "http://192.168.25.136        :8000";

    public static final String time_get_url = host_url + "/time/";
    public static final String time_field_str = "time";

    public static final String get_code_post_url = host_url + "/signup/";
    public static final String get_password = host_url + "/user/password/reset/";
    public static final String email_field_str = "email";

    public static final String login_url = host_url + "/login/";
    public static final String password_field_str = "password";

    public static final String get_personalInfo_url = host_url + "/user/me/retrieve/";

    public static final int USERNAME_NOT_MATH = 1;

    public static final int INVALID_USERNAME = 2;
    public static final int USERNAME_BLANK = 3;
    public static final int PASSWORD_BLANK = 4;
    public static final int VALIDATED = 0;

    // Shared preferences
    public static final String REQUEST_SERVER_TIME = "server_time";
    public static final String request_time_field = "request_time"; //for register
    public static final String request_time_field2 = "request_time2"; //for forget password

    public static final String USER_DETAIL = "user_detail";
    public static final String user_field = "user";
    public static final String token_field = "token";


    // Constants need by Shop fragment
    public static final int CATERGORYAMOUNT = 5;
    public static final String GALLERY_ABS_PATH = "/Gallery/Category";
    public static final int INITIAL_END_POS = 8;

}

