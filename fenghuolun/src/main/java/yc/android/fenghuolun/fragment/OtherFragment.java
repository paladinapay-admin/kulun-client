package yc.android.fenghuolun.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import yc.android.fenghuolun.R;
import yc.android.fenghuolun.activity.AboutUsActivity;
import yc.android.fenghuolun.activity.CustomerServiceActivity;
import yc.android.fenghuolun.activity.LoginActivity;
import yc.android.fenghuolun.activity.ModifyPersonalInfoActivity;

public class OtherFragment extends Fragment{

    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final Button op1_general_setting;
        final Button op2_general_setting;
        final Button op3_general_setting;

        if (rootView == null)
        {
            rootView = inflater.inflate(R.layout.other_fragment, null);

            op1_general_setting = (Button) rootView.findViewById(R.id.btn_general_setting);
            op1_general_setting.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Toast.makeText(rootView.getContext(),
                            "此功能尚在研发之中，敬请期待",
                            Toast.LENGTH_LONG).show();

                }
            });

            op2_general_setting = (Button) rootView.findViewById(R.id.btn_customer_service);
            op2_general_setting.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent i = new Intent(getActivity(), CustomerServiceActivity.class);
                    startActivity(i);


                }
            });


            op3_general_setting = (Button) rootView.findViewById(R.id.btn_about_us);
            op3_general_setting.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent i = new Intent(getActivity(), AboutUsActivity.class);
                    startActivity(i);

                }
            });

        }
        // 缓存的rootView需要判断是否已经被加过parent，如果有parent需要从parent删除，
        // 要不然会发生这个rootview已经有parent的错误。
        ViewGroup parent = (ViewGroup) rootView.getParent();
        if (parent != null)
        {
            parent.removeView(rootView);
        }
        return rootView;
    }
}