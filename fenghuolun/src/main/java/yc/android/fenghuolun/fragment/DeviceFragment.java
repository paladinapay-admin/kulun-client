package yc.android.fenghuolun.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.method.CharacterPickerDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;

import java.util.ArrayList;
import java.util.List;

import yc.android.fenghuolun.R;
import yc.android.fenghuolun.activity.MainActivity;
import yc.android.fenghuolun.activity.ScannerActivity;
import yc.android.fenghuolun.adapter.DeviceAdapter;
import yc.android.fenghuolun.adapter.DeviceItem;
import yc.android.fenghuolun.communicator.DeviceFragmentCommunicator;

import static android.view.View.OnClickListener;

/**
 * Created by Yichang on 2014/6/16.
 */
public class DeviceFragment extends Fragment implements DeviceFragmentCommunicator {

    private static final String TAG = "fenghuolun";


    private List<DeviceItem> deviceItemList;
    private DeviceAdapter deviceAdapter;

    private SwipeListView swipeListView;

    private String deviceDefaultName;
    private String deviceDefaultDescription;
    private Drawable deviceDefaultIcon;

    private View rootView;

    //private ActivityCommunicator activityCommunicator;
//
//    public DeviceFragment(){
//        setHasOptionsMenu(true);
//    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //Connect the deviceFragmentCommunicator variable in MainActivity to this fragment
        ((MainActivity) getActivity()).deviceFragmentCommunicator = this;
        //Connect activityCommunicator in this class to MainActivity

        //activityCommunicator = (ActivityCommunicator) (MainActivity)getActivity();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //setHasOptionsMenu(true);

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.device_fragment_layout, null);

            deviceItemList = new ArrayList<DeviceItem>();

            swipeListView = (SwipeListView) rootView.findViewById(R.id.device_list);

            DisplayMetrics dm = getResources().getDisplayMetrics();
            int px_fragment_width = dm.widthPixels;
            float px_btn_width = getResources().getDimensionPixelSize(R.dimen.btn_width);
            swipeListView.setOffsetLeft(px_fragment_width - 2 * px_btn_width);
            //swipeListView.setOffsetRight(px_fragment_width);

            ColorDrawable divider_color = new ColorDrawable(getResources().getColor(R.color.divider));
            swipeListView.setDivider(divider_color);
            swipeListView.setDividerHeight(3);

            deviceDefaultName = getResources().getString(R.string.device_default_name);
            deviceDefaultDescription = getResources().getString(R.string.device_default_description);
            deviceDefaultIcon = getResources().getDrawable(R.drawable.app_logo);

            deviceAdapter = new DeviceAdapter(this.getActivity(), deviceItemList, swipeListView);

            for (int i = 0; i < ((MainActivity) getActivity()).getBTDevices().size(); i++) {
                DeviceItem item = new DeviceItem();
                item.setDeviceName(((MainActivity) getActivity()).getBTDevices().get(i).getDeviceName());
                item.setDeviceDescription(((MainActivity) getActivity()).getBTDevices().get(i).getDeviceAddress());
                item.setIcon(deviceDefaultIcon);

                deviceItemList.add(item);

                //swipeListView.closeOpenedItems();

                deviceAdapter.notifyDataSetChanged();
            }

            final AlertDialog.Builder builder = new AlertDialog.Builder(
                    getActivity());
            //builder.setIcon(R.drawable.ic_launcher);
            builder.setTitle("添加方式");
            final String[] cities = {"二维码扫描","手动输入", "服务器同步"};
            builder.setItems(cities, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    //TODO let user to select the way to add (QR or sync from server)
                    //sorry I hard code this

                    dialog.dismiss();

                    if (which == 0){ //二维码

                        //TODO check if can be returned back
                        Intent intent = new Intent(getActivity(), ScannerActivity.class);
                        startActivity(intent);

                    }
                    else if(which == 1) { //手动输入

                    }
                    else if (which ==2) {
                        //服务器
                    }

                }
            });


            swipeListView.setSwipeListViewListener(new BaseSwipeListViewListener() {

                @Override
                public void onOpened(int position, boolean toRight) {
                }

                @Override
                public void onClosed(int position, boolean fromRight) {
                }

                @Override
                public void onListChanged() {
                }

//            @Override
//            public void onMove(int position, float x) {
//            }
//
//            @Override
//            public void onStartOpen(int position, int action, boolean right) {
//                Log.d(TAG, String.format("onStartOpen %d - action %d", position, action));
//            }
//
//            @Override
//            public void onStartClose(int position, boolean right) {
//                Log.d(TAG, String.format("onStartClose %d", position));
//            }

                @Override
                public void onClickFrontView(int position) {
                    if (((MainActivity) getActivity()).getBluetoothStatus()) {
                        Log.d(TAG, String.format("onClickFrontView %d", position));
                        ((MainActivity) getActivity()).setCurrDeviceIndex(position);
                        //remove the tab menu
                        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0, 0);
                        getActivity().findViewById(android.R.id.tabhost).setLayoutParams(param);

                        //add a new fragment without destroying the current fragment
                        DeviceItem item = deviceItemList.get(position);
                        String address = item.getDeviceDescription();
                        Log.e(TAG, "Selected BT address is " + address);
                        //activityCommunicator.passStringToActivity(((MainActivity) getActivity()).GO_TO_PROGRAM_UI, address);
                        ((MainActivity) getActivity()).connectBluetoothService(address);

                        /* Commented out by Sasha
                        AddProgrammeFragment addProgrammeFragment = new AddProgrammeFragment();
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction t = fragmentManager.beginTransaction();
                        t.add(R.id.real_tab_content, addProgrammeFragment).commit();
                        */

                        ProgramFragment programFragment = new ProgramFragment();
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction t = fragmentManager.beginTransaction();
                        t.add(R.id.real_tab_content, programFragment).commit();
                    }
                }

                @Override
                public void onPressedFrontView(int position) {
                    Log.d(TAG, String.format("onPressedFrontView %d", position));
                    swipeListView.closeOpenedItems();
                }

                @Override
                public void onReleasedFrontView(int position) {
                    Log.d(TAG, String.format("onReleasedFrontView %d", position));
                }

                @Override
                public void onClickBackView(int position) {
                    Log.d(TAG, String.format("onClickBackView %d", position));
                }

                @Override
                public void onPressedBackView(int position) {
                    Log.d(TAG, String.format("onPressedBackView %d", position));

                }

                @Override
                public void onReleasedBackView(int position) {
                    Log.d(TAG, String.format("onReleasedBackView %d", position));
                }

                @Override
                public void onDismiss(int[] reverseSortedPositions) {
                }

            });

            Button btnAddDevice = (Button) rootView.findViewById(R.id.btn_add_device);

            btnAddDevice.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return swipeListView.getTouchListener().isPaused();
                }
            });

            btnAddDevice.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    builder.show();

                    //((MainActivity) getActivity()).searchBluetoothDevice();

                    /*
                    DeviceItem item = new DeviceItem();

                    int count = deviceItemList.size() + 1;

                    item.setDeviceName(deviceDefaultName + count);
                    item.setDeviceDescription(deviceDefaultDescription);
                    item.setIcon(deviceDefaultIcon);

                    deviceItemList.add(item);

                    //swipeListView.closeOpenedItems();

                    deviceAdapter.notifyDataSetChanged();

                    //swipeListView.setSelection(count - 1);
                    //swipeListView.requestFocus();
                    */
                }
            });

            swipeListView.setAdapter(deviceAdapter);


        }
        // 缓存的rootView需要判断是否已经被加过parent，如果有parent需要从parent删除，
        // 要不然会发生这个rootview已经有parent的错误。
        ViewGroup parent = (ViewGroup) rootView.getParent();
        if (parent != null) {
            parent.removeView(rootView);
        }
        return rootView;
    }

    @Override
    public void passDevicesToFragment(String BTDeviceName, String BTDeviceAddr) {

        DeviceItem item = new DeviceItem();

        item.setDeviceName(BTDeviceName);
        item.setDeviceDescription(BTDeviceAddr);
        item.setIcon(deviceDefaultIcon);

        deviceItemList.add(item);

        //swipeListView.closeOpenedItems();

        deviceAdapter.notifyDataSetChanged();


        //swipeListView.setSelection(count - 1);
        //swipeListView.requestFocus();

    }


//    public void clearAllDevices(){
//        deviceItemList.clear();
//        deviceAdapter.notifyDataSetChanged();
//    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
//        inflater.inflate(R.menu.device_menu, menu);
//        super.onCreateOptionsMenu(menu,inflater);
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item){
//        switch (item.getItemId()){
//            case R.id.clear_all_devices:
//                ((MainActivity)getActivity()).clearDevice();
//                clearAllDevices();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }
}