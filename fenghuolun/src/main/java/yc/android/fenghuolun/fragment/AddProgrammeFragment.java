package yc.android.fenghuolun.fragment;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

import yc.android.fenghuolun.R;
import yc.android.fenghuolun.activity.MainActivity;

/**
 * Created by yichangzhang on 23/08/2014.
 */
public class AddProgrammeFragment extends Fragment {

    public Context context;
    //private ActivityCommunicator activityCommunicator;

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        context = getActivity();
        //activityCommunicator = (ActivityCommunicator) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        View v = inflater.inflate(R.layout.add_programme_fragment_layout, container, false);

        v.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        Button btn_return2programme = (Button) v.findViewById(R.id.btn_return2programme);

        btn_return2programme.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //restore the tab menu
//                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
//                        LinearLayout.LayoutParams.MATCH_PARENT,
//                        LinearLayout.LayoutParams.WRAP_CONTENT);
//                getActivity().findViewById(android.R.id.tabhost).setLayoutParams(param);

                //remove the current fragment
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction t = fragmentManager.beginTransaction();
                t.remove(AddProgrammeFragment.this).commit();
            }
        });

        Button btnSendNewProgram = (Button) v.findViewById(R.id.send_new_program);
        btnSendNewProgram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //((MainActivity)getActivity()).sendProgramHeaderToDevice();
                Toast.makeText(getActivity().getApplicationContext(),"This button is currently not effective", Toast.LENGTH_SHORT).show();
            }
        });

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        if (menu != null) {
            menu.clear();
        }
        inflater.inflate(R.menu.add_program_menu, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.send_program_1:
                ((MainActivity)getActivity()).sendProgram(1);
                return true;
            case R.id.send_program_2:
                ((MainActivity)getActivity()).sendProgram(2);
                return true;
            case R.id.send_program_3:
                ((MainActivity)getActivity()).sendProgram(3);
                return true;
            case R.id.send_sample_config:
                ((MainActivity)getActivity()).sendBikeConfig(28,false);
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}