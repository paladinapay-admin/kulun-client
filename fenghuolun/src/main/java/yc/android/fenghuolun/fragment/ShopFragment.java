package yc.android.fenghuolun.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;


import yc.android.fenghuolun.R;
import yc.android.fenghuolun.adapter.GridviewImageLoadAdapter;
import yc.android.fenghuolun.utils.RetrieveWebPageSource;
import yc.android.fenghuolun.view.PullToRefreshView;
import yc.android.fenghuolun.view.PullToRefreshView.OnFooterRefreshListener;
import yc.android.fenghuolun.view.PullToRefreshView.OnHeaderRefreshListener;
import yc.android.fenghuolun.constant.Constant;

/**
 * Created by yichangzhang on 25/08/2014.
 * Modified by peizhao from 02/10/2014
 */
public class ShopFragment extends Fragment implements RadioGroup.OnCheckedChangeListener,
        OnFooterRefreshListener,OnHeaderRefreshListener {
    private RadioGroup mRadioGroup;
    private RadioButton mRadioButton1;
    private RadioButton mRadioButton2;
    private RadioButton mRadioButton3;
    private RadioButton mRadioButton4;
    private RadioButton mRadioButton5;
    private float radio_btn_width;
    private ImageView mImageView;
    private float mCurrentCheckedRadioLeft;
    private HorizontalScrollView mHorizontalScrollView;
    private ViewPager mViewPager;
    private ArrayList<View> mViews;
    private ProgressDialog pd;
    private View rootView;
    private HashMap<Integer, GridView> grids = new HashMap<Integer, GridView>();


    private HashMap<Integer, ArrayList<String>> category_index_urls
            = new HashMap<Integer, ArrayList<String>>();
    private HashMap<Integer, Integer> category_index_endPositions = new HashMap<Integer, Integer>();
    private HashMap<Integer, PullToRefreshView> mPullToRefreshViews =
            new HashMap<Integer, PullToRefreshView>();

    //Following are declarations needed by gridview and reder pics stuff



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //TODO Setup ProgressDialog
//        pd = new ProgressDialog(getActivity());
//        pd.setMessage("正在载入。。。");
//        pd.setCancelable(true);

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.shop_fragment, null);

//            pd.show();
            createPicsDir(); //create the required gallery dirs
            iniController();
            iniListener();
            iniVariable(inflater);
            mRadioButton1.setChecked(true);
            mViewPager.setCurrentItem(0);
            initialise_refresh_btn();
            mCurrentCheckedRadioLeft = getCurrentCheckedRadioLeft();

        }

//        pd.dismiss();

        // 缓存的rootView需要判断是否已经被加过parent，如果有parent需要从parent删除，
        // 要不然会发生这个rootview已经有parent的错误。
        ViewGroup parent = (ViewGroup) rootView.getParent();
        if (parent != null) {
            parent.removeView(rootView);
            //TODO check network

            AnimationSet animationSet = new AnimationSet(true);
            TranslateAnimation translateAnimation = new TranslateAnimation(mCurrentCheckedRadioLeft, mCurrentCheckedRadioLeft, 0f, 0f);
            animationSet.addAnimation(translateAnimation);
            animationSet.setFillBefore(false);
            animationSet.setFillAfter(true);
            animationSet.setDuration(100);
            mImageView.startAnimation(animationSet);
        }

        return rootView;
    }

    private void initialise_refresh_btn() {
        ImageButton refreshbtn = (ImageButton) rootView.findViewById(R.id.reload_img_btn);
        refreshbtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                //Refresh cache directory downloaded images
                if (!isNetworkConnected(getActivity().getBaseContext())) {
//                    TextView tv = (TextView) mViews.get(mViewPager.getCurrentItem()).findViewById(R.id.noNetwork);
//                    tv.setVisibility(View.VISIBLE);
                    GridView gv = (GridView) mViews.get(mViewPager.getCurrentItem()).findViewById(R.id.grid_imgs_gallery);
                    gv.setVisibility(View.INVISIBLE);

                } else {
//                    TextView tv = (TextView) mViews.get(mViewPager.getCurrentItem()).findViewById(R.id.noNetwork);
//                    tv.setVisibility(View.INVISIBLE);
                    GridView gv = (GridView) mViews.get(mViewPager.getCurrentItem()).findViewById(R.id.grid_imgs_gallery);
                    gv.setVisibility(View.VISIBLE);

                    int current_item = mViewPager.getCurrentItem();

                    GridviewImageLoadAdapter tempAdapter = (GridviewImageLoadAdapter)
                            (grids.get(current_item).getAdapter());
                    tempAdapter.getImageLoader().clearCache();
                    tempAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    private void iniVariable(LayoutInflater inflater) {
        mViews = new ArrayList<View>();
        mViews.add(inflater.inflate(R.layout.category_1, null));
        mViews.add(inflater.inflate(R.layout.category_2, null));
        mViews.add(inflater.inflate(R.layout.category_3, null));
        mViews.add(inflater.inflate(R.layout.category_4, null));
        mViews.add(inflater.inflate(R.layout.category_5, null));
        mViewPager.setAdapter(new MyPagerAdapter());
    }

    private void iniController() {

        radio_btn_width = getResources().getDimension(R.dimen.radio_btn_width);
        mRadioGroup = (RadioGroup) rootView.findViewById(R.id.radioGroup);
        mRadioButton1 = (RadioButton) rootView.findViewById(R.id.btn1);
        mRadioButton2 = (RadioButton) rootView.findViewById(R.id.btn2);
        mRadioButton3 = (RadioButton) rootView.findViewById(R.id.btn3);
        mRadioButton4 = (RadioButton) rootView.findViewById(R.id.btn4);
        mRadioButton5 = (RadioButton) rootView.findViewById(R.id.btn5);

        mImageView = (ImageView) rootView.findViewById(R.id.img_indicator);

        mHorizontalScrollView = (HorizontalScrollView) rootView
                .findViewById(R.id.horizontalScrollView);

        mViewPager = (ViewPager) rootView.findViewById(R.id.pager);
    }

    private void iniListener() {

        mRadioGroup.setOnCheckedChangeListener(this);
        mViewPager.setOnPageChangeListener(new MyPagerOnPageChangeListener());
    }

    private float getCurrentCheckedRadioLeft() {
        if (mRadioButton1.isChecked()) {
            return 0;
        } else if (mRadioButton2.isChecked()) {

            return radio_btn_width;
        } else if (mRadioButton3.isChecked()) {

            return radio_btn_width * 2;
        } else if (mRadioButton4.isChecked()) {

            return radio_btn_width * 3;
        } else if (mRadioButton5.isChecked()) {

            return radio_btn_width * 4;
        }
        return 0f;
    }

    private void createPicsDir() {
        String pdir = getActivity().getFilesDir().getPath();
//        File SDCardRoot = new File(pdir+"Gallery/Category" + Integer.toString(category_index)+"/");
//        Toast.makeText(activity,"file dirs are"+SDCardRoot,Toast.LENGTH_LONG).show();

        File gallery = new File(pdir + Constant.GALLERY_ABS_PATH );

        // have the object build the directory structure, if needed.
        gallery.mkdirs();

//        Toast.makeText(getActivity(),"file dirs are "+gallery.getAbsolutePath(),Toast.LENGTH_LONG).show();

        for (int i = 1; i < Constant.CATERGORYAMOUNT + 1; i++) {
            File category = new File(pdir+ Constant.GALLERY_ABS_PATH + String.valueOf(i));
            category.mkdirs();

        }
    }

    private void initialise_grids(int c_position){
        //Initialise the grid and adapter per view page and add them into the arrays

        if (!grids.containsKey(c_position)) {
            PullToRefreshView mPullToRefreshView = (PullToRefreshView)mViews.get(c_position)
                    .findViewById(R.id.main_pull_refresh_view);
            mPullToRefreshView.setOnFooterRefreshListener(this);
            mPullToRefreshView.setOnHeaderRefreshListener(this);

            mPullToRefreshViews.put(c_position, mPullToRefreshView);

            GridView grid = (GridView) mViews.get(c_position).findViewById(R.id.grid_imgs_gallery);
            // TODO waiting
            // TODO return null
            ArrayList<String> sub_category_imgs_strs = analyse_url(
                    "http://182.254.149.159/category_"
                    + Integer.toString(c_position + 1) + "/");

            category_index_urls.put(c_position, sub_category_imgs_strs);


            ArrayList<String> initial_data_this_category = new ArrayList<String>();
            for (int i = 0; i < Constant.INITIAL_END_POS && i < sub_category_imgs_strs.size(); i++){
                initial_data_this_category.add(sub_category_imgs_strs.get(i));
            }
            category_index_endPositions.put(c_position,Constant.INITIAL_END_POS + 8);
//            GridviewImageLoadAdapter adapter = new GridviewImageLoadAdapter(this.getActivity(), sub_category_imgs_str, position + 1);
            GridviewImageLoadAdapter adapter = new GridviewImageLoadAdapter(this.getActivity(), c_position + 1);
            adapter.setCurrentData(initial_data_this_category);
            grid.setAdapter(adapter);

            grids.put(c_position, grid);
        }
    }

    private ArrayList<String> analyse_url(String category_url) {

        ArrayList<String> imgages_str_inside_category = new ArrayList<String>();

        RetrieveWebPageSource rwps = new RetrieveWebPageSource();
        try {
            String entirepage = rwps.execute(category_url).get();
//            Toast.makeText(getActivity(),entirepage , Toast.LENGTH_LONG).show();

            String [] images_str = entirepage.split("</tr>");
            for (int i = 3; i < images_str.length; i++ ){
                String img_to_display_p1 = "";
                if (images_str[i].length() > 78 ){
                   img_to_display_p1 = images_str[i].substring(images_str[i].indexOf(">",79)+1,images_str[i].indexOf("<",78));
                }
//                String img_to_display_p2 = img_to_display_p1.substring(img_to_display_p1.indexOf(">",10)+1);
                if (!img_to_display_p1.trim().equals("")){
                    imgages_str_inside_category.add(category_url+img_to_display_p1.trim());
                }

//                Toast.makeText(getActivity(),img_to_display_p2 , Toast.LENGTH_LONG).show();
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


        return imgages_str_inside_category;

    }



    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation translateAnimation;

        if (checkedId == R.id.btn1) {
            translateAnimation = new TranslateAnimation(mCurrentCheckedRadioLeft, 0, 0f, 0f);
            animationSet.addAnimation(translateAnimation);
            animationSet.setFillAfter(true);
            animationSet.setDuration(100);
            mImageView.startAnimation(animationSet);
            mViewPager.setCurrentItem(0);
            initialise_grids(0);
        } else if (checkedId == R.id.btn2) {
            translateAnimation = new TranslateAnimation(mCurrentCheckedRadioLeft, radio_btn_width, 0f, 0f);
            animationSet.addAnimation(translateAnimation);
            animationSet.setFillAfter(true);
            animationSet.setDuration(100);
            mImageView.startAnimation(animationSet);
            mViewPager.setCurrentItem(1);
            initialise_grids(1);
        } else if (checkedId == R.id.btn3) {
            translateAnimation = new TranslateAnimation(mCurrentCheckedRadioLeft, radio_btn_width * 2, 0f, 0f);
            animationSet.addAnimation(translateAnimation);
            animationSet.setFillAfter(true);
            animationSet.setDuration(100);
            mImageView.startAnimation(animationSet);
            mViewPager.setCurrentItem(2);
            initialise_grids(2);
        } else if (checkedId == R.id.btn4) {
            translateAnimation = new TranslateAnimation(mCurrentCheckedRadioLeft, radio_btn_width * 3, 0f, 0f);
            animationSet.addAnimation(translateAnimation);
            animationSet.setFillAfter(true);
            animationSet.setDuration(100);
            mImageView.startAnimation(animationSet);
            mViewPager.setCurrentItem(3);
            initialise_grids(3);
        } else if (checkedId == R.id.btn5) {
            translateAnimation = new TranslateAnimation(mCurrentCheckedRadioLeft, radio_btn_width * 4, 0f, 0f);
            animationSet.addAnimation(translateAnimation);
            animationSet.setFillAfter(true);
            animationSet.setDuration(100);
            mImageView.startAnimation(animationSet);
            mViewPager.setCurrentItem(4);
            initialise_grids(4);
        }

        mCurrentCheckedRadioLeft = getCurrentCheckedRadioLeft();

        mHorizontalScrollView.smoothScrollTo((int) mCurrentCheckedRadioLeft - (int) radio_btn_width, 0);
    }



    private class MyPagerAdapter extends PagerAdapter {

        @Override
        public void destroyItem(View v, int position, Object obj) {
            ((ViewPager) v).removeView(mViews.get(position));
        }

        @Override
        public void finishUpdate(View arg0) {
        }

        @Override
        public int getCount() {
            return mViews.size();
        }

        @Override
        public Object instantiateItem(View v, int position) {
            ((ViewPager) v).addView(mViews.get(position));
            return mViews.get(position);
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public void startUpdate(View arg0) {
        }

    }

    private class MyPagerOnPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageSelected(int position) {

            if (position == 0) {
                mRadioButton1.performClick();
            } else if (position == 1) {
                mRadioButton2.performClick();
            } else if (position == 2) {
                mRadioButton3.performClick();
            } else if (position == 3) {
                mRadioButton4.performClick();
            } else if (position == 4) {
                mRadioButton5.performClick();
            }
        }

    }

    @Override
    public void onFooterRefresh(PullToRefreshView view) {
        final int current_item = mViewPager.getCurrentItem();

        final PullToRefreshView mPullToRefreshView = mPullToRefreshViews.get(current_item);

        mPullToRefreshView.postDelayed(new Runnable() {

            @Override
            public void run() {
                mPullToRefreshView.onFooterRefreshComplete();
//                int current_item = mViewPager.getCurrentItem();


                ArrayList<String> current_dataset = category_index_urls.get(current_item);


                ArrayList<String> next_data_this_category = new ArrayList<String>();

                for (int i = 0; i < category_index_endPositions.get(current_item)
                        && i < current_dataset.size(); i++){
                    next_data_this_category.add(current_dataset.get(i));
                }

                category_index_endPositions.put(current_item,Constant.INITIAL_END_POS + 8);

                GridviewImageLoadAdapter tempAdapter = (GridviewImageLoadAdapter)
                        (grids.get(current_item).getAdapter());
                tempAdapter.setCurrentData(next_data_this_category);
//                tempAdapter.getImageLoader().clearCache();

                tempAdapter.notifyDataSetChanged();
            }
        }, 2000);
    }

    @Override
    public void onHeaderRefresh(PullToRefreshView view) {
        final int current_item = mViewPager.getCurrentItem();

        final PullToRefreshView mPullToRefreshView = mPullToRefreshViews.get(current_item);

        mPullToRefreshView.postDelayed(new Runnable() {
            @Override
            public void run() {
                // 设置更新时间
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);
                int hour = c.get(Calendar.HOUR_OF_DAY);
                int minute = c.get(Calendar.MINUTE);
                mPullToRefreshView.onHeaderRefreshComplete("最近更新: "
                        + Integer.toString(year)+"年"
                        + Integer.toString(month)+"月"
                        + Integer.toString(day)+"日 "
                        + Integer.toString(hour)+"时"
                        + Integer.toString(minute)+"分");

                ArrayList<String> initial_data_this_category = new ArrayList<String>();
                ArrayList<String> current_urls = category_index_urls.get(current_item);
                int max_num = current_urls.size();
                for (int i = 0; i < Constant.INITIAL_END_POS && i < max_num; i++){
                    initial_data_this_category.add(current_urls.get(i));
                }
                category_index_endPositions.put(current_item,Constant.INITIAL_END_POS + 8);




                GridviewImageLoadAdapter tempAdapter = (GridviewImageLoadAdapter)
                        (grids.get(current_item).getAdapter());


                tempAdapter.flush_previous_data();
                tempAdapter.setCurrentData(initial_data_this_category);
                tempAdapter.getImageLoader().clearCache();
                tempAdapter.notifyDataSetChanged();
//				mPullToRefreshView.onHeaderRefreshComplete();
            }
        },2000);

    }

    private boolean isNetworkConnected(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            if (mNetworkInfo != null) {
                return mNetworkInfo.isAvailable();
            }
        }
        return false;
    }

}