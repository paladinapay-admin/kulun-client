package yc.android.fenghuolun.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import yc.android.fenghuolun.R;
import yc.android.fenghuolun.activity.LoginActivity;
import yc.android.fenghuolun.activity.ModifyPasswordActivity;
import yc.android.fenghuolun.activity.ModifyPersonalInfoActivity;
import yc.android.fenghuolun.constant.Constant;

public class MyselfFragment extends Fragment {

    private View rootView;
    private static String unable_to_connect_server_str;
    private static String timeout_str;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        unable_to_connect_server_str = getResources().
                getString(R.string.unable_to_connect_server_str);
        timeout_str = getResources().getString(R.string.timeout_str);


       final TextView personal_info_ln;
       final TextView personal_info_fn;
       final TextView personal_info_phone;
       final TextView personal_info_address;
       final TextView personal_info_avai_pic;
       final TextView personal_info_avai_device;

        if (rootView == null) {

            rootView = inflater.inflate(R.layout.myself_fragment_layout, null);
            personal_info_ln = (TextView) rootView.findViewById(
                    R.id.mine_last_name_display);
            personal_info_fn = (TextView) rootView.findViewById
                    (R.id.mine_first_name_display);
            personal_info_phone = (TextView) rootView.findViewById(
                    R.id.mine_phone_display);
            personal_info_address = (TextView) rootView.findViewById(
                    R.id.mine_address_display);

            // TODO Currently API N/A on server
            personal_info_avai_pic = (TextView) rootView.findViewById(
                    R.id.mine_available_pic_display);

            personal_info_avai_device = (TextView) rootView.findViewById(
                    R.id.mine_available_device_display);

            final Activity thisActivity = getActivity();


//            String tk = "1430cbab080591134b0e8b5231705ad78622c969"; // Later need to be grabbed from sharepereferance
            SharedPreferences prefs = rootView.getContext().getSharedPreferences(
                    Constant.USER_DETAIL, 0);

            String tk = prefs.getString(Constant.token_field,"");
            String username = prefs.getString(Constant.user_field, "");

            AsyncHttpClient client = new AsyncHttpClient();
            client.addHeader("Authorization", "Token " + tk);

            RequestParams params = new RequestParams();
            params.put(Constant.user_field, username);

//            Toast.makeText(rootView.getContext(),
//                    "Before client onSuccess",
//                    Toast.LENGTH_SHORT).show();


            client.get(Constant.get_personalInfo_url, params, new JsonHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                      JSONObject errorResponse) {

                    if (statusCode ==0){
                        Toast.makeText(getActivity(), timeout_str, Toast.LENGTH_SHORT).show();
                    }
                    else {

                        Toast.makeText(rootView.getContext(),
                                unable_to_connect_server_str ,
                                Toast.LENGTH_SHORT).show();
                    }


                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

//                    Toast.makeText(rootView.getContext(),
//                            "Inside client onSuccess",
//                            Toast.LENGTH_SHORT).show();
                    String ln_str = null;
                    String fn_str = null;
                    String phone_str = null;
                    String address_str = null;
//                    String ava_device_pic TBD
                    int ava_devices_num = 0;
                    int ava_pics_num = 0;

                    try {
                        ln_str = response.getString("last_name");
                        fn_str = response.getString("first_name");
                        phone_str = response.getString("phone");
                        address_str = response.getString("address");
                        ava_devices_num = response.getJSONArray("devices").length();
                        ava_pics_num = response.getJSONArray("pictures").length();


//                        Toast.makeText(rootView.getContext(),
//                                "length is "+Integer.toString(ava_devices_num),
//                                Toast.LENGTH_SHORT).show();
//                        ava_device_num = response.getJSONArray("devices");

//                        ava_device_num = response.getString();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                    personal_info_ln.setText(ln_str);
                    personal_info_fn.setText(fn_str);
                    personal_info_phone.setText(phone_str);
                    personal_info_address.setText(address_str);
                    personal_info_avai_device.setText(Integer.toString(ava_devices_num));
                    personal_info_avai_pic.setText(Integer.toString(ava_pics_num));


                    super.onSuccess(statusCode, headers, response);
                }
            });


            Button btnModifyPersonalInfo = (Button) rootView.findViewById(
                    R.id.btn_mine_modify_personal_info);
            btnModifyPersonalInfo.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent i = new Intent(thisActivity, ModifyPersonalInfoActivity.class);
                    startActivity(i);
                }
            });


            Button btnModifyPassword = (Button) rootView.findViewById(
                    R.id.btn_mine_modify_password);
            btnModifyPassword.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent i = new Intent(thisActivity, ModifyPasswordActivity.class);
                    startActivity(i);
                }
            });

            Button btnLogout = (Button) rootView.findViewById(R.id.btn_mine_logout);

            btnLogout.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v) {

                    SharedPreferences prefs = thisActivity.getSharedPreferences(Constant.USER_DETAIL, 0);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(Constant.user_field, "");
                    editor.putString(Constant.token_field, "");
                    editor.commit();

                    thisActivity.finish();
                    Intent i = new Intent(thisActivity, LoginActivity.class);
                    startActivity(i);
                }
            });

        }
        // 缓存的rootView需要判断是否已经被加过parent，如果有parent需要从parent删除，
        // 要不然会发生这个rootview已经有parent的错误。
        ViewGroup parent = (ViewGroup) rootView.getParent();
        if (parent != null) {
            parent.removeView(rootView);
        }


        return rootView;
    }


    private void displayPersonalInfo(View rootView) {

    }
}