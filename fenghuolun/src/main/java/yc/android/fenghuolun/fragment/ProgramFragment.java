package yc.android.fenghuolun.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import yc.android.fenghuolun.R;
import yc.android.fenghuolun.activity.MainActivity;
import yc.android.fenghuolun.adapter.CheckboxListAdapter;
import yc.android.fenghuolun.communicator.ProgramFragmentCommunicator;
import yc.android.fenghuolun.adapter.XHLPrograms;


public class ProgramFragment extends Fragment implements ProgramFragmentCommunicator {

    public Context context;
    //private ActivityCommunicator activityCommunicator;
    private String activityAssignedValue = null;
    private TextView mConnectionStatus;
    private TextView mReceivedMessage;

    private TextView speed;
    private TextView distance;

    private TextView title;

    private ListView programListView;
    //private ArrayAdapter<String> programArrayAdapter;
    private CheckboxListAdapter adapter;

    private Button addProgram;

    private ProgressBar usageBar;
    private TextView usageBarText;

    public ProgramFragment(){
        setHasOptionsMenu(true);
    }




    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        context = getActivity();
        //activityCommunicator = (ActivityCommunicator) context;
        ((MainActivity)context).programFragmentCommunicator = this;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        setHasOptionsMenu(true);

        View view = inflater.inflate(R.layout.program_fragment_layout,
                container, false);

        view.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        programListView = (ListView) view.findViewById(R.id.program_list);

        Button btn_return2device = (Button) view.findViewById(R.id.btn_return2device);
        btn_return2device.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //activityCommunicator.passStringToActivity(((MainActivity)getActivity()).DISCONNECT_BT_DEVICE,"PlaceHolder");
                ((MainActivity)getActivity()).disconnectBtDevice();

                //restore the tab menu
                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                getActivity().findViewById(android.R.id.tabhost).setLayoutParams(param);

                //remove the current fragment
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction t = fragmentManager.beginTransaction();
                t.remove(ProgramFragment.this).commit();
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        title = (TextView) this.getView().findViewById(R.id.program_fragment_title);
        title.setText(R.string.main_title_programs);

        mConnectionStatus = (TextView) this.getView().findViewById(R.id.connection_status);
        mConnectionStatus.setText(((MainActivity)getActivity()).getBtConnectionStatus());


        //mReceivedMessage = (TextView) this.getView().findViewById(R.id.received_message);

        addProgram = (Button) this.getView().findViewById(R.id.add_new_program);
        addProgram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //activityCommunicator.passStringToActivity(2,"place holder");
                AddProgrammeFragment addProgrammeFragment = new AddProgrammeFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction t = fragmentManager.beginTransaction();
                t.add(R.id.real_tab_content, addProgrammeFragment).commit();
            }
        });

        usageBar = (ProgressBar) this.getView().findViewById(R.id.usage_bar);
        usageBarText = (TextView) this.getView().findViewById(R.id.usage_bar_text);
        usageBarText.setText(getString(R.string.cannot_get_usage));
        int usage = ((MainActivity) getActivity()).getUsage();
        if (usage >= 0 && usage <= 100) {
            usageBar.setProgress(usage);
            usageBarText.setText(usage + "%");

        }

        speed = (TextView) this.getView().findViewById(R.id.speed);
        distance = (TextView) this.getView().findViewById(R.id.distance);

    }
    /*
    public void showConnectionStatus(String connectionStatus){
        Toast.makeText(getActivity().getApplicationContext(),"a",Toast.LENGTH_SHORT).show();
        //mConnectionStatus.setText(connectionStatus);
    }*/

    @Override
    public void passStringToFragment(String someValue, int whichTextView){
        activityAssignedValue = someValue;
        if (whichTextView == 1) {
            mConnectionStatus.setText(activityAssignedValue);
        }else if (whichTextView == 2){
            //mReceivedMessage.setText(activityAssignedValue);
            mReceivedMessage.append(activityAssignedValue);
        }else if(whichTextView == 3){
            speed.setText(someValue);
        }else if(whichTextView == 4){
            distance.setText(someValue);
        }

    }

    @Override
    public void passIntToFragment(int someValue){
        usageBar.setProgress(someValue);
        usageBarText.setText(someValue + "%");
    }

    @Override
    public void passProgramToFragment(ArrayList<XHLPrograms> programs){
        //programArrayAdapter.clear();

        /*
        XHLPrograms program = new XHLPrograms("XYY",5,false);
        ArrayList<XHLPrograms> programs = new ArrayList<XHLPrograms>();
        programs.add(program);
        */

        adapter = new CheckboxListAdapter(getActivity(),programs);
        programListView.setAdapter(adapter);
        /*
        adapter.clear();
        for(int i=0; i<programs.size();i++){
            //programArrayAdapter.add(programs.get(i));
            adapter.add("item");
        }*/

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        if (menu != null) {
            menu.clear();
        }
        inflater.inflate(R.menu.program_menu, menu);
        super.onCreateOptionsMenu(menu,inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.refresh:
                ((MainActivity)getActivity()).requestProgramsBT();
                return true;
            case R.id.delete_selected:
                if(adapter != null){
                    ArrayList<Integer> selectedPrograms= adapter.getSelectedPrograms();
                    ((MainActivity)getActivity()).deleteSelectedPrograms(selectedPrograms);
                }else{
                    Toast.makeText(getActivity().getApplicationContext(),R.string.program_not_loaded,Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.play_selected:
                if(adapter != null){
                    ArrayList<Integer> selectedPrograms= adapter.getSelectedPrograms();
                    ((MainActivity)getActivity()).playSelectedPrograms(selectedPrograms);
                }else{
                    Toast.makeText(getActivity().getApplicationContext(),R.string.program_not_loaded,Toast.LENGTH_SHORT).show();
                }
                return true;

            case R.id.connect:
                ((MainActivity)getActivity()).connectBtDevice();
                return true;
            case R.id.disconnect:
                ((MainActivity)getActivity()).disconnectBtDevice();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
