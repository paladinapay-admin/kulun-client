package yc.android.fenghuolun.communicator;

import java.util.ArrayList;

import yc.android.fenghuolun.adapter.XHLPrograms;

/**
 * Created by sasha on 31/08/14.
 */
public interface ProgramFragmentCommunicator {

    public void passProgramToFragment(ArrayList<XHLPrograms> programs);

    public void passStringToFragment(String someValue, int whichTextView);

    public void passIntToFragment(int someValue);
}
