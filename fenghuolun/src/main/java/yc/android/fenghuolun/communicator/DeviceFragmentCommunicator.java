package yc.android.fenghuolun.communicator;

/**
 * Created by sasha on 30/08/14.
 */
public interface DeviceFragmentCommunicator {
    public void passDevicesToFragment(String BTDeviceName, String BTDeviceAddr);

}
