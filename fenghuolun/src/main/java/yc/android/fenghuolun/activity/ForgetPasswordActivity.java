package yc.android.fenghuolun.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.*;

import java.math.BigDecimal;
import java.util.regex.Pattern;

import yc.android.fenghuolun.constant.Constant;
import yc.android.fenghuolun.R;

public class ForgetPasswordActivity extends Activity {

    private static MyCounter timer;

    private static boolean isRunning = false;

    private static String duplicate_get_code_error_str;
    private static String unable_to_connect_server_str;
    private static String no_network_str;
    private static String waiting_str;
    private static String get_password_success_str;
    private static String username_not_match_str;
    private static String invalid_username_str;
    private static String username_blank_str;
    private static String internal_error_str;
    private static String no_email_str;
    private static String timeout_str;

    private ProgressDialog pd;

    private Context context;

    private AsyncHttpClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_forget_password);

        context = getApplicationContext();

        client = new AsyncHttpClient();

        duplicate_get_code_error_str = getResources().
                getString(R.string.duplicate_get_code_error_str);

        unable_to_connect_server_str = getResources().
                getString(R.string.unable_to_connect_server_str);

        waiting_str = getResources().
                getString(R.string.waiting_str);

        no_network_str = getResources().
                getString(R.string.no_network_str);

        get_password_success_str = getResources().
                getString(R.string.get_password_success_str);

        username_not_match_str = getResources().
                getString(R.string.username_not_match_str);

        invalid_username_str = getResources().
                getString(R.string.invalid_username_str);

        username_blank_str = getResources().
                getString(R.string.username_blank_str);

        internal_error_str = getResources().
                getString(R.string.internal_error_str);

        no_email_str = getResources().
                getString(R.string.no_email_str);

        timeout_str = getResources().getString(R.string.timeout_str);

        //Animation
        Animation myAnimation = AnimationUtils.loadAnimation(this, R.anim.login_anim);
        ImageView cycleImg = (ImageView) findViewById(R.id.cycle_img);
        cycleImg.setAnimation(myAnimation);

        final TextView email_textView = (TextView) findViewById(R.id.forget_password_username_edit);
        final TextView email_textView2 = (TextView) findViewById(R.id.forget_password_username2_edit);

        Button btn_get_password =
                (Button) findViewById(R.id.btn_get_password);
        btn_get_password.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {

                final String email_str = email_textView.getText().toString().
                        toLowerCase().trim();

                String email_str_2 = email_textView2.getText().toString().
                        toLowerCase().trim();

                if (has_error_message(email_str, email_str_2)) {
                    return;
                }

                if (!isRunning) {

                    // ProgressDialog
                    pd = ProgressDialog.show(ForgetPasswordActivity.this, "", waiting_str);

                    SharedPreferences prefs = getSharedPreferences(Constant.REQUEST_SERVER_TIME, 0);
                    String preServerTimeStr = prefs.getString(Constant.request_time_field2, "0");
                    final BigDecimal preServerTimeFloat = new BigDecimal(preServerTimeStr);

                    client.get(Constant.time_get_url, new JsonHttpResponseHandler() {

                        @Override
                        public void onSuccess(int statusCode, Header[] headers,
                                              JSONObject response) {

                            String responseStr;

                            try {
                                responseStr = response.getString(Constant.time_field_str);

                                BigDecimal responseFloat = new BigDecimal(responseStr);

                                BigDecimal diffFloat = responseFloat.subtract(preServerTimeFloat);

                                int interval_in_second =
                                        Constant.interval_between_two_get_code_requests / 1000;

                                if (diffFloat.compareTo(
                                        new BigDecimal(interval_in_second)) > 0) {

                                    saveRequestServerTime(responseStr);

                                    timer = new MyCounter(
                                            Constant.interval_between_two_get_code_requests,
                                            Constant.tick_interval);
                                    timer.start();

                                    requestActiveCode(email_str);

                                } else {
                                    pd.dismiss();
                                    Toast.makeText(context,
                                            duplicate_get_code_error_str,
                                            Toast.LENGTH_SHORT).show();

                                    // count the remaining time
                                    timer = new MyCounter((interval_in_second -
                                            Long.valueOf(diffFloat.toBigInteger().toString()))
                                            * 1000,
                                            Constant.tick_interval);
                                    timer.start();
                                }

                            } catch (JSONException je) {
                                pd.dismiss();
                                Toast.makeText(context,
                                        internal_error_str,
                                        Toast.LENGTH_SHORT).show();
                            }

                            super.onSuccess(statusCode, headers, response);
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers,
                                              Throwable throwable, JSONObject errorResponse) {
                            pd.dismiss();
                            if (statusCode ==0){
                                Toast.makeText(context, timeout_str, Toast.LENGTH_SHORT).show();
                            }
                            else {
                                Toast.makeText(context,
                                        unable_to_connect_server_str,
                                        Toast.LENGTH_SHORT).show();
                            }
                            super.onFailure(statusCode, headers, throwable, errorResponse);
                        }

                    });
                } else {
                    Toast.makeText(context, duplicate_get_code_error_str,
                            Toast.LENGTH_SHORT).show();
                }
            }

        });

    }

    private boolean has_error_message(String email_str, String email_str_2){

        if (!isNetworkConnected(context)) {
            Toast.makeText(context,
                    no_network_str, Toast.LENGTH_SHORT).show();
            return true;
        }

        int validator_result = validator(email_str, email_str_2);

        if (validator_result == Constant.INVALID_USERNAME) {
            Toast.makeText(context, invalid_username_str,
                    Toast.LENGTH_SHORT).show();
            return true;
        }
        else if (validator_result == Constant.USERNAME_BLANK) {
            Toast.makeText(context, username_blank_str,
                    Toast.LENGTH_SHORT).show();
            return true;
        }
        else if (validator_result == Constant.USERNAME_NOT_MATH) {
            Toast.makeText(context, username_not_match_str,
                    Toast.LENGTH_SHORT).show();
            return true;
        }

        return false;
    }

    private boolean emailValidator(String email){
        return Pattern.compile(Constant.EMAIL_PATTERN).matcher(email).matches();
    }

    private int validator(String email, String email2){

        if (email.equals("")){
            return Constant.USERNAME_BLANK;
        }
        else if (email2.equals("")){
            return Constant.USERNAME_BLANK;
        }
        else if (!emailValidator(email)){
            return Constant.INVALID_USERNAME;
        }
        else if (!emailValidator(email2)){
            return Constant.INVALID_USERNAME;
        }
        else if (!email.equals(email2)) {
            return Constant.USERNAME_NOT_MATH;
        }
        return Constant.VALIDATED;
    }

    private void requestActiveCode(String email){
        RequestParams params = new RequestParams();
        params.put(Constant.email_field_str, email);

        client.post(Constant.get_password, params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                pd.dismiss();

                Toast.makeText(context, get_password_success_str,
                        Toast.LENGTH_LONG).show();
                Intent i = new Intent(ForgetPasswordActivity.this, LoginActivity.class);
                TextView txtViewUsername = (TextView) findViewById(R.id.forget_password_username_edit);
                i.putExtra("username", txtViewUsername.getText().toString());
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody,
                                  Throwable error) {
                pd.dismiss();

                if (statusCode ==0){
                    Toast.makeText(context, timeout_str, Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(context, no_email_str,
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void saveRequestServerTime(String serverTime) {
        SharedPreferences prefs = getSharedPreferences(Constant.REQUEST_SERVER_TIME, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constant.request_time_field2, serverTime);
        editor.commit();
    }

    private boolean isNetworkConnected(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            if (mNetworkInfo != null) {
                return mNetworkInfo.isAvailable();
            }
        }
        return false;
    }

    private class MyCounter extends CountDownTimer {

        public MyCounter(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            isRunning = false;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            isRunning = true;
        }
    }
}