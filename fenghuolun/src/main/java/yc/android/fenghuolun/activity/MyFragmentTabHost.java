package yc.android.fenghuolun.activity;

import android.content.Context;
import android.support.v4.app.FragmentTabHost;
import android.util.AttributeSet;

/**
 * Created by yichangzhang on 23/08/2014.
 */
public class MyFragmentTabHost extends FragmentTabHost {

    public MyFragmentTabHost(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyFragmentTabHost(Context context) {
        super(context);
    }

    @Override
    public void onTouchModeChanged(boolean isInTouchMode) {
    }
}

