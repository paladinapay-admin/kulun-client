package yc.android.fenghuolun.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

import yc.android.fenghuolun.constant.Constant;
import yc.android.fenghuolun.R;

/**
 * Created by yichangzhang on 15/08/2014.
 */
public class LoginActivity extends Activity{

    private long exitTime;
    private static String press_again_to_exit;
    private TextView textView_email;
    private TextView textView_password;

    private ProgressDialog pd;

    private Context context;

    private static String no_network_str;
    private static String invalid_username_str;
    private static String username_blank_str;
    private static String password_blank_str;
    private static String waiting_str;
    private static String login_fail_str;
    private static String internal_error_str;
    private static String timeout_str;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        context = getApplicationContext();

        no_network_str = getResources().
                getString(R.string.no_network_str);

        invalid_username_str = getResources().
                getString(R.string.invalid_username_str);

        username_blank_str = getResources().
                getString(R.string.username_blank_str);

        password_blank_str = getResources().
                getString(R.string.password_blank_str);

        waiting_str = getResources().
                getString(R.string.waiting_str);

        press_again_to_exit = getResources().
                getString(R.string.press_again_to_exit);

        login_fail_str = getResources().
                getString(R.string.login_fail_str);

        internal_error_str = getResources().
                getString(R.string.internal_error_str);

        timeout_str = getResources().getString(R.string.timeout_str);

        textView_email = (TextView)findViewById(R.id.username_edit);
        textView_password = (TextView)findViewById(R.id.password_edit);

        //Animation
        Animation myAnimation = AnimationUtils.loadAnimation(this, R.anim.login_anim);
        ImageView cycleImg = (ImageView) findViewById(R.id.cycle_img);
        cycleImg.setAnimation(myAnimation);

        Intent i = getIntent();
        textView_email.setText(i.getStringExtra("username"));

        Button btnLogin = (Button) findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final String email_str = textView_email.getText().toString().
                        toLowerCase().trim();

                String password_str = textView_password.getText().toString().trim();

                if (has_error_message(email_str, password_str)){
                    return;
                }

                pd = ProgressDialog.show(LoginActivity.this, "", waiting_str);

                RequestParams params = new RequestParams();
                params.put(Constant.email_field_str, email_str);
                params.put(Constant.password_field_str, password_str);

                AsyncHttpClient client = new AsyncHttpClient();

                client.post(Constant.login_url, params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers,
                                          JSONObject response) {
                        try {
                            pd.dismiss();
                            String token = response.getString(Constant.token_field);
                            saveUserDetail(email_str, token);
                            finish();
                            Intent i = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(i);

                        } catch (JSONException e) {
                            pd.dismiss();
                            Toast.makeText(context,
                                    internal_error_str,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers,
                                          Throwable throwable, JSONObject errorResponse) {
                        pd.dismiss();
                        if (statusCode == 0){
                            Toast.makeText(context, timeout_str, Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(getApplicationContext(), login_fail_str,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        //Register button click handler
        Button btnRegister = (Button) findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });

        Button btnForgetPassword = (Button) findViewById(R.id.btn_forget_password);
        btnForgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                startActivity(i);
            }
        });
    }

    private void saveUserDetail(String username, String token) {
        SharedPreferences prefs = getSharedPreferences(Constant.USER_DETAIL, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constant.user_field, username);
        editor.putString(Constant.token_field, token);
        editor.commit();
    }

    private boolean isNetworkConnected(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            if (mNetworkInfo != null) {
                return mNetworkInfo.isAvailable();
            }
        }
        return false;
    }

    private boolean has_error_message(String email_str, String password_str){

        if (!isNetworkConnected(context)) {
            Toast.makeText(context,
                    no_network_str, Toast.LENGTH_SHORT).show();
            return true;
        }

        int validator_result = validator(email_str, password_str);

        if (validator_result == Constant.INVALID_USERNAME) {
            Toast.makeText(context, invalid_username_str,
                    Toast.LENGTH_SHORT).show();
            return true;
        }
        else if (validator_result == Constant.USERNAME_BLANK) {
            Toast.makeText(context, username_blank_str,
                    Toast.LENGTH_SHORT).show();
            return true;
        }
        else if (validator_result == Constant.PASSWORD_BLANK) {
            Toast.makeText(context, password_blank_str,
                    Toast.LENGTH_SHORT).show();
            return true;
        }

        return false;
    }

    private boolean emailValidator(String email){
        return Pattern.compile(Constant.EMAIL_PATTERN).matcher(email).matches();
    }

    private int validator(String email, String password){

        if (email.equals("")){
            return Constant.USERNAME_BLANK;
        }
        else if (!emailValidator(email)){
            return Constant.INVALID_USERNAME;
        }
        else if (password.equals("")){
            return Constant.PASSWORD_BLANK;
        }
        return Constant.VALIDATED;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN)
        {

            //System.currentTimeMillis()无论何时调用，肯定大于2000
            if((System.currentTimeMillis()-exitTime) > 2000)
            {
                Toast.makeText(getApplicationContext(), press_again_to_exit,
                        Toast.LENGTH_SHORT).show();
                exitTime = System.currentTimeMillis();
            }
            else
            {
                finish();
                //System.exit(0);
            }

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
