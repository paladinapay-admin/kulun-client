package yc.android.fenghuolun.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.NumberPicker;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

import yc.android.fenghuolun.R;

public class ConfigActivity extends Activity {
    private int index;
    private int wheelSize;
    private boolean milageOn;


    private NumberPicker numberPicker;
    private Switch milageSwitch;
    private Button OK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        setResult(Activity.RESULT_CANCELED);

        Bundle extras = getIntent().getExtras();
        index = extras.getInt("index");
        wheelSize = extras.getInt("wheelSize");
        milageOn = extras.getBoolean("milageOn");


        numberPicker = (NumberPicker) findViewById(R.id.number_picker);
        numberPicker.setMaxValue(30);
        numberPicker.setMinValue(24);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setDescendantFocusability(numberPicker.FOCUS_BLOCK_DESCENDANTS);
        numberPicker.setValue(wheelSize);

        milageSwitch = (Switch) findViewById(R.id.milage_switch);
        milageSwitch.setChecked(milageOn);//the default value is true;
        milageSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    milageOn = true;
                    Toast.makeText(getApplicationContext(),"milage on",Toast.LENGTH_SHORT).show();
                }else{
                    milageOn = false;
                    Toast.makeText(getApplicationContext(),"milage off",Toast.LENGTH_SHORT).show();
                }
            }
        });

        OK = (Button) findViewById(R.id.OK_button);
        OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wheelSize = numberPicker.getValue();
                Intent intent = new Intent();
                Bundle extras = new Bundle();
                extras.putInt("index",index);
                extras.putInt("wheelSize",wheelSize);
                extras.putBoolean("milageOn",milageOn);
                intent.putExtras(extras);
                setResult(Activity.RESULT_OK,intent);
                finish();

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.config, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
