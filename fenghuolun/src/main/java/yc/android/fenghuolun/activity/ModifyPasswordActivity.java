package yc.android.fenghuolun.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import yc.android.fenghuolun.R;

/**
 * Created by zhaopei on 2/10/2014.
 */
public class ModifyPasswordActivity extends Activity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.modify_personal_password);

        Button btnModifyPasswordConfirm = (Button) findViewById (R.id.modify_password_confirm);
        btnModifyPasswordConfirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //TODO check if login detail is valid
//                Intent i = new Intent(ModifyPasswordActivity.this, TryDynamicListActivity.class);
//                Intent i = new Intent(ModifyPasswordActivity.this, DynamicGridViewActivity.class);
//                startActivity(i);
                finish();
                //Toast.makeText(v.getContext(), "button login is clicked", Toast.LENGTH_SHORT).show();
            }
        });

        Button btnModifyPasswordCancel = (Button) findViewById (R.id.modify_password_cancel);
        btnModifyPasswordCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //TODO check if login detail is valid

                finish();
                //Toast.makeText(v.getContext(), "button login is clicked", Toast.LENGTH_SHORT).show();
            }
        });

    }



}
