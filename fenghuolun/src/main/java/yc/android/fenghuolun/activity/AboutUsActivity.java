package yc.android.fenghuolun.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Window;
import android.widget.ImageView;

import java.io.File;

import yc.android.fenghuolun.R;

public class AboutUsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.about_us);

        ImageView iv = (ImageView) findViewById(R.id.test_storage);
        String pdir = this.getFilesDir().getPath();
        File imgFile = new File(pdir+"/Gallery/Category2/2.jpg");
        if(imgFile.exists()){

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());


            iv.setImageBitmap(myBitmap);

        }

    }
}
