package yc.android.fenghuolun.activity;


import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;

import yc.android.fenghuolun.R;
import yc.android.fenghuolun.communicator.DeviceFragmentCommunicator;
import yc.android.fenghuolun.communicator.ProgramFragmentCommunicator;
import yc.android.fenghuolun.fragment.*;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.concurrent.LinkedBlockingQueue;

import yc.android.fenghuolun.bluetooth.BluetoothService;
import yc.android.fenghuolun.adapter.XHLPrograms;
import yc.android.fenghuolun.fragment.DeviceFragment;

/**
 * Created by yichangzhang on 15/08/2014.
 */
public class MainActivity extends FragmentActivity {

    // Debugging
    private static final String TAG = "fenghuolun";

    // Intent request codes
    private static final int REQUEST_ENABLE_BT = 1;
    private static final int REQUEST_CONNECT_DEVICE = 2;
    private static final int REQUEST_CONFIG_DEVICE = 3;

    // Message types sent from the BluetoothService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;

    // Key names received from the BluetoothService Handler
    public static final String DEVICE_NAME = "device_name";

    // Message types sent from the intepretBTMessagethread
    private static final int LIST_PROGRAMS = 1;
    private static final int REQUEST_PROGRAMS = 2;
    private static final int SEND_NEXT_PICTURE = 3;
    private static final int FAIL_TO_RECEIVE = 4;
    private static final int CONFIG_UPDATED = 5;
    private static final int MILAGE_UPDATE = 6;
    private static final int PROGRAM_TO_PLAY_UPDATED = 7;
    private static final int SYNC_CONFIG = 8;


    // interface through which communication is made to fragment
    public DeviceFragmentCommunicator deviceFragmentCommunicator;
    public ProgramFragmentCommunicator programFragmentCommunicator;

    // Shared preferences
    private static final String STORED_BT_DEVICES = "bluetooth_devices";

    private String btConnectionStatus;

    //Keeps all historically selected BT devices until manually deleted
    private ArrayList<BTDevice> deviceArrayList;
    private static final int DEFAULT_WHEEL_SIZE = 26;
    private static final boolean MILAGE_ON = true;

    private int usage = -1; // 储存连接的蓝牙设备中已用的节目位

    // Keep track of the currently active BT device
    private String selectedBTDeviceAddr;
    private int selectedBTDeviceIndex = -1;

    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothService mBTService = null;

    private String mConnectedDeviceName = null;

    private LinkedBlockingQueue<PairElement> receivedMsgQueue;
    private InterpretBTMessageThread btReceivingThread;

    private LinkedBlockingQueue<byte[]> pictures;
    private CountDownTimer sendPicCountDown;
    private CountDownTimer sendConfigCountDown;
    private CountDownTimer sendProgramUpdateCountDown;

    private ArrayList<XHLPrograms> xhlPrograms;
    private ArrayList<Integer> programsToBePlayed;

    private long exitTime;
    private String press_again_to_exit;

    //定义FragmentTabHost对象
    private FragmentTabHost mTabHost;

    //定义一个布局
    private LayoutInflater layoutInflater;

    //定义数组来存放Fragment界面
    //private Class fragmentArray[] = {DeviceFragment.class, AddProgrammeFragment.class,
    //        FragmentPage3.class, OtherFragment.class};
    private Class fragmentArray[] = {DeviceFragment.class, ShopFragment.class,
            MyselfFragment.class, OtherFragment.class};

    //定义数组来存放按钮图片
    //TODO let artist design those icons
    private int mImageViewArray[] = {R.drawable.tab_device_btn, R.drawable.tab_gallery_btn,
            R.drawable.tab_myself_btn, R.drawable.tab_others_btn};

    //Tab选项卡的文字
    private String mTextViewArray[];


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG, "+++ On Create +++");

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        press_again_to_exit = getResources().
                getString(R.string.press_again_to_exit);

        initView();

        //Get local Bluetooth Adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        deviceArrayList = new ArrayList<BTDevice>();

        restoreCurrDeviceArrayList();
        /*
        Toast.makeText(getApplicationContext(), "Startup deviceArrayList size is " +
                deviceArrayList.size(), Toast.LENGTH_SHORT).show();
        */
        if (mBluetoothAdapter == null) {
            Toast.makeText(getApplicationContext(),
                    getResources().getText(R.string.bluetooth_not_supported).toString(),
                    Toast.LENGTH_LONG).show();
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            }
        }
    }


    @Override
    public void onDestroy() {
        saveCurrDeviceArrayList();
        if (mBTService != null) mBTService.stop();
        super.onDestroy();

        Log.e(TAG, "--- ON DESTROY ---");
    }

    /*
        Save the current deviceArrayList to shared preference
     */
    private void saveCurrDeviceArrayList() {
        Log.e(TAG, "Saving current device array list");
        SharedPreferences prefs = getSharedPreferences(STORED_BT_DEVICES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("numberOfElement", deviceArrayList.size());
        for (int i = 0; i < deviceArrayList.size(); i++) {

            editor.putString(String.valueOf(i)+"deviceName",
                    deviceArrayList.get(i).getDeviceName());
            editor.putString(String.valueOf(i)+"deviceAddress",
                    deviceArrayList.get(i).getDeviceAddress());

            editor.putInt(String.valueOf(i)+"wheelSize",
                    deviceArrayList.get(i).getWheelSize());

            editor.putBoolean(String.valueOf(i)+"milageOn",
                    deviceArrayList.get(i).isMilageOn());
        }

        editor.commit();

    }

    /*
        Restore the deviceArrayList from shared preference
     */
    private void restoreCurrDeviceArrayList() {
        Log.e(TAG, "Restoring current device array list");
        SharedPreferences prefs = getSharedPreferences(STORED_BT_DEVICES, Context.MODE_PRIVATE);

        int numberOfElements = prefs.getInt("numberOfElement", -1);
        //deviceArrayList = new ArrayList<String>();
        for (int i = 0; i < numberOfElements; i++) {
            String deviceName = prefs.getString(String.valueOf(i)+"deviceName", "");
            String deviceAddress = prefs.getString(String.valueOf(i)+"deviceAddress", "");
            int wheelSize = prefs.getInt(String.valueOf(i)+"wheelSize", 26);
            boolean milageOn = prefs.getBoolean(String.valueOf(i)+"milageOn", true);
            BTDevice device = new BTDevice(deviceName,deviceAddress,wheelSize,milageOn);
            deviceArrayList.add(device);
        }
    }



    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(getApplicationContext(), R.string.bluetooth_enabled,
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.bluetooth_not_enabled,
                            Toast.LENGTH_SHORT).show();
                }
                break;
            case REQUEST_CONNECT_DEVICE:
                if (resultCode == Activity.RESULT_OK) {
                    selectedBTDeviceAddr = data.getExtras().
                            getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    BluetoothDevice device = mBluetoothAdapter.
                            getRemoteDevice(selectedBTDeviceAddr);
                    addBluetoothDevice(device.getName(), selectedBTDeviceAddr);
                    /*
                    Toast.makeText(getApplicationContext(), "Device " +
                                    selectedBTDeviceAddr + " is selected",
                            Toast.LENGTH_SHORT).show();
                    */

                }
                break;
            case REQUEST_CONFIG_DEVICE:
                if (resultCode == Activity.RESULT_OK){
                    Bundle bundle = data.getExtras();
                    int index = bundle.getInt("index");
                    int wheelSize = bundle.getInt("wheelSize");
                    boolean milageOn = bundle.getBoolean("milageOn");

                    BTDevice device = deviceArrayList.get(index);
                    device.setWheelSize(wheelSize);
                    device.setMilageOn(milageOn);
                    deviceArrayList.set(index,device);

                }
                break;
        }
    }



    /**
     * 初始化组件
     */
    private void initView() {

        mTextViewArray = new String[]{getResources().getString(R.string.main_tab_device),
                getResources().getString(R.string.main_tab_gallery),
                getResources().getString(R.string.main_tab_myself),
                getResources().getString(R.string.main_tab_other)};


        //实例化布局对象
        layoutInflater = LayoutInflater.from(this);

        //实例化TabHost对象，得到TabHost
        mTabHost = (MyFragmentTabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), R.id.real_tab_content);

        //得到fragment的个数
        int count = fragmentArray.length;

        for (int i = 0; i < count; i++) {
            //为每一个Tab按钮设置图标、文字和内容
            TabSpec tabSpec = mTabHost.newTabSpec(mTextViewArray[i]).
                    setIndicator(getTabItemView(i));
            //将Tab按钮添加进Tab选项卡中
            mTabHost.addTab(tabSpec, fragmentArray[i], null);
        }
    }



    /**
     * 给Tab按钮设置图标和文字
     */
    private View getTabItemView(int index) {
        View view = layoutInflater.inflate(R.layout.tab_item_layout, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.tab_icon_image);
        imageView.setImageResource(mImageViewArray[index]);

        TextView textView = (TextView) view.findViewById(R.id.tab_icon_text);
        textView.setText(mTextViewArray[index]);

        return view;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {

            //System.currentTimeMillis()无论何时调用，肯定大于2000
            if ((System.currentTimeMillis() - exitTime) > 2000) {
                Toast.makeText(getApplicationContext(), press_again_to_exit,
                        Toast.LENGTH_SHORT).show();
                exitTime = System.currentTimeMillis();
            } else {
                // save BT device list to shared preference
                saveCurrDeviceArrayList();
                // close BT connection
                if (mBTService != null) mBTService.stop();
                finish();
                System.exit(0);
            }

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }



    /*
     * Will be obsolete as the device will be added via the server using the validation code
     * Searches for nearby bluetooth devices (currently search for ALL BT devices)
     * This method is being called from Device Fragment when add new device button is pressed
     *
     */
    public void searchBluetoothDevice() {
        if (mBluetoothAdapter == null) {
            //btnAddDevice.setEnabled(false);
            Toast.makeText(getApplicationContext(), getResources().getText(
                    R.string.bluetooth_not_supported).toString(), Toast.LENGTH_LONG).show();
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                Toast.makeText(getApplicationContext(), getResources().getText(
                        R.string.bluetooth_not_enabled).toString(), Toast.LENGTH_LONG).show();
                //Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                //startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            } else { // if BT is already enabled
                Intent btIntent = new Intent(this, DeviceListActivity.class);
                startActivityForResult(btIntent, REQUEST_CONNECT_DEVICE);
            }
        }

    }

    public void configDevice(int index){
        Intent configIntent = new Intent(this, ConfigActivity.class);
        Bundle extras = new Bundle();
        extras.putInt("index",index);
        extras.putInt("wheelSize",deviceArrayList.get(index).getWheelSize());
        extras.putBoolean("milageOn",deviceArrayList.get(index).isMilageOn());
        configIntent.putExtras(extras);
        startActivityForResult(configIntent,REQUEST_CONFIG_DEVICE);
    }

    public void connectBluetoothService(String address){ // UI 6

        Log.d(TAG,"enterProgramFragment Connecting to" + address);
        mBTService = new BluetoothService(this, mHandler);
        BluetoothDevice mBluetoothDevice = mBluetoothAdapter.getRemoteDevice(address);

        mBTService.connect(mBluetoothDevice);

        selectedBTDeviceAddr = address;

    }

    /*
        Used by DeviceFragement.java to check the status of the bluetooth device
     */
    public boolean getBluetoothStatus(){
        if (mBluetoothAdapter != null) {
            return mBluetoothAdapter.isEnabled();
        }else{
            return false;
        }
    }

    /*
        Construct a object BTDevice and added to the deviceArrayList
     */
    private void addBluetoothDevice(String deviceName, String address) {
        BTDevice device = new BTDevice(deviceName,address,DEFAULT_WHEEL_SIZE, MILAGE_ON);
        Log.e(TAG,"Current Device Name "+deviceName + " Address " + address);
        boolean alreadyExist = false;
        for(int i=0; i<deviceArrayList.size();i++){
            if (deviceArrayList.get(i).getDeviceName().equals(deviceName) &&
                    deviceArrayList.get(i).getDeviceAddress().equals(address)){
                alreadyExist = true;
            }
        }

        if (alreadyExist == true) {
            Log.d(TAG, "deviceArrayList already contains " + deviceName + '\n' + address);
        } else {
            deviceArrayList.add(device);
            Log.d(TAG, "is deviceFragmentCommunicator null ?");
            if (deviceFragmentCommunicator != null) {
                Log.d(TAG, "deviceFragmentCommunicator is not null");
                deviceFragmentCommunicator.passDevicesToFragment(deviceName, address);
            } else {
                Log.d(TAG, "deviceFragmentCommunicator is null");
            }
        }

    }


    public ArrayList<BTDevice> getBTDevices() { return deviceArrayList; }


    public String getBtConnectionStatus() {
        return btConnectionStatus;
    }

    public void setCurrDeviceIndex(int index){
        selectedBTDeviceIndex = index;
    }


    public void clearDevice() {
        Log.d(TAG, "clearing deviceArrayList in MainActivity");
        if(deviceArrayList != null) {
            deviceArrayList.clear();
        }
    }

    public void connectBtDevice(){
        if (mBTService == null) {
            connectBluetoothService(selectedBTDeviceAddr);
        }
        else if (mBTService.getState() != BluetoothService.STATE_CONNECTED) {
            connectBluetoothService(selectedBTDeviceAddr);
        }
    }

    public void disconnectBtDevice(){
        if(mBTService != null){
            Log.e(TAG,"Stopping mBTService");
            mBTService.stop();
            mBTService = null;
        }
    }


    /*
        returns a percentage to the nearest whole number
     */
    public int getUsage(){
        int usagePercentage = -1;
        if(usage <= 20){
            usagePercentage = usage * 5;
        }

        return usagePercentage;
    }


    // The Handler that gets information back from BluetoothService
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    switch (msg.arg1) {

                        case BluetoothService.STATE_CONNECTED:
                            btConnectionStatus = getString(R.string.title_connected_to) + mConnectedDeviceName;
                            if (programFragmentCommunicator != null) {
                                programFragmentCommunicator.passStringToFragment(
                                        btConnectionStatus, 1);

                            }
                            sendHandShakeDataBT();

                            receivedMsgQueue = new LinkedBlockingQueue<PairElement>();
                            btReceivingThread = new InterpretBTMessageThread(msgHandler);
                            btReceivingThread.start();
                            break;
                        case BluetoothService.STATE_CONNECTING:
                            btConnectionStatus = getString(R.string.title_connecting);
                            if (programFragmentCommunicator != null) {
                                programFragmentCommunicator.passStringToFragment(
                                        btConnectionStatus, 1);
                                //connectionStatus.setText(R.string.title_connecting);
                            }
                            break;
                        case BluetoothService.STATE_LISTEN:
                            break;
                        case BluetoothService.STATE_NONE:
                            Log.d(TAG,"Bluetooth Disconnected");
                            Toast.makeText(getApplicationContext(),R.string.bluetooth_disconnected,Toast.LENGTH_LONG).show();
                            btConnectionStatus = getString(R.string.title_not_connected);
                            if (programFragmentCommunicator != null) {
                                programFragmentCommunicator.passStringToFragment(
                                        btConnectionStatus, 1);
                                //connectionStatus.setText(R.string.title_not_connected);
                            }
                            if(btReceivingThread != null){
                                btReceivingThread.terminate();
                                btReceivingThread = null;
                            }
                            if(pictures != null) {
                                pictures.clear(); // if disconnected during sending pictures to device, clear the queue
                            }
                            break;
                    }
                    break;
                case MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    //String writeMessage = new String(writeBuf);
                    //mBTMessageArrayAdapter.add("Me: " + writeMessage);
                    break;
                case MESSAGE_READ:
                    byte[] readMessageByte = (byte[]) msg.obj;
                    //String readMessageStr = new String(readMessageByte, 0, msg.arg1);
                    PairElement pairElement = new PairElement(readMessageByte,msg.arg1);
                    receivedMsgQueue.add(pairElement);

                    Log.e(TAG,"length of received byte array  is "+ readMessageByte.length);

                    //receiveMessage(readMessageStr, readMessageByte);
                    //programFragmentCommunicator.passStringToFragment(readMessageStr,2);
                    //receiveMessageString(readMessage);
                    //Log.d(TAG,"received msg: " + readMessage);

                /*
                if (fragmentCommunicator != null){
                    Log.d(TAG, "in MESSAGE_READ passStringToFragment");
                    Log.d(TAG, "received message is "+ readMessage);
                    fragmentCommunicator.passStringToFragment(readMessage, 2);
                }*/

                    break;
                case MESSAGE_DEVICE_NAME:
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(getApplicationContext(), getString(R.string.connected_to_prefix)
                            + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    break;

            }
        }
    };


    private final Handler msgHandler = new Handler(){
        public void handleMessage(Message msg){
            switch(msg.what){
                case LIST_PROGRAMS:
                    Log.e(TAG,"in msgHandler LIST_PROGRAMS");
                    loadExistingPrograms((byte[]) msg.obj);
                    if(sendProgramUpdateCountDown !=null) {
                        sendProgramUpdateCountDown.cancel();
                    }
                    break;
                case REQUEST_PROGRAMS:
                    Log.d(TAG,"requestProgramBT");
                    requestProgramsBT();
                    break;
                case SEND_NEXT_PICTURE:
                    // temporary for testing purpose
                    Log.d(TAG,"In SEND_NEXT_PICTURE");
                    if(sendPicCountDown != null) {
                        sendPicCountDown.cancel();
                    }
                    if (mBTService.getState() == BluetoothService.STATE_CONNECTED) {
                        if(pictures.size()>0) {
                            Log.d(TAG, "Sending next picture...");
                            String headStr = "#XHLB9";
                            byte[] head = headStr.getBytes();
                            mBTService.write(head);

                            try {
                                byte[] picture = pictures.take();
                                mBTService.write(picture);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            String tailStr = "#XHLE9\r\n";
                            byte[] tail = tailStr.getBytes();
                            mBTService.write(tail);

                            sendPicCountDown = new CountDownTimer(10000,1000) {
                                @Override
                                public void onTick(long millisUntilFinished) {

                                }

                                @Override
                                public void onFinish() {
                                    Toast.makeText(getApplicationContext(),
                                            R.string.send_picture_failed,Toast.LENGTH_LONG).show();
                                    pictures.clear();
                                }
                            }.start();
                        } else if(pictures.size() == 0){
                            Toast.makeText(getApplicationContext(),R.string.sending_program_completed,Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                case FAIL_TO_RECEIVE:
                    Toast.makeText(getApplicationContext(),
                            R.string.send_picture_failed,Toast.LENGTH_LONG).show();
                    pictures.clear();
                    break;
                case CONFIG_UPDATED:
                    sendConfigCountDown.cancel();
                    Toast.makeText(getApplicationContext(),
                            R.string.config_updated,Toast.LENGTH_LONG).show();
                    requestProgramsBT();
                    break;
                case MILAGE_UPDATE:
                    byte[] speed = Arrays.copyOfRange((byte[]) msg.obj,5,10);
                    byte[] distance = Arrays.copyOfRange((byte[]) msg.obj,11,16);
                    try {
                        String speedStr = new String(speed, "UTF-8");
                        String distanceStr = new String(distance, "UTF-8");

                        if (programFragmentCommunicator != null) {
                            programFragmentCommunicator.passStringToFragment(
                                    speedStr, 3);

                            programFragmentCommunicator.passStringToFragment(
                                    distanceStr, 4);
                        }

                    }catch(UnsupportedEncodingException e){
                        Toast.makeText(getApplicationContext(),
                                R.string.decoding_failed,Toast.LENGTH_LONG).show();
                    }
                    break;
                case PROGRAM_TO_PLAY_UPDATED:
                    Toast.makeText(getApplicationContext(),
                            R.string.program_to_play_updated,Toast.LENGTH_SHORT).show();
                    updatePlayingProgram();
                    break;
                case SYNC_CONFIG:
                    if (selectedBTDeviceIndex != -1) {
                        int wheelSize = deviceArrayList.get(selectedBTDeviceIndex).getWheelSize();
                        boolean milageOn = deviceArrayList.get(selectedBTDeviceIndex).isMilageOn();
                        sendBikeConfig(wheelSize,milageOn);
                    }
                    break;
            }
        }
    };

    private void loadExistingPrograms(byte[] receivedMsg) {
        usage = 0; // reset the counter for 节目位
        int numOfPrograms = (receivedMsg[6] & 0xFF) | 0x00 << 8 | 0x00 << 16 | 0x00 << 24;

        Log.d(TAG, "int repeat is: " + numOfPrograms);
        String receivedMsgStr = new String(receivedMsg);
        Log.e(TAG,"receivedMsgStr is " + receivedMsgStr);
        int offset = 7;
        //ArrayList temp = new ArrayList<String>();
        xhlPrograms = new ArrayList<XHLPrograms>();
        try {
            for (int i = 0; i < numOfPrograms; i++) {
                byte[] programName = Arrays.copyOfRange(receivedMsg, offset, offset + 8);
                String programNameStr = new String(programName, 0, 8);
                Log.d(TAG, "nameStr is: " + programNameStr);
                offset += 8;
                int numOfPics = (receivedMsg[offset + 1] & 0xFF) |
                        ((receivedMsg[offset] & 0xFF) << 8) | (0x00 << 16) | (0x00 << 24);
                Log.d(TAG, "numOfPics is: " + numOfPics);
                offset += 2;
                int durationPerPic = (receivedMsg[offset + 1] & 0xFF) |
                        (receivedMsg[offset] & 0xFF) << 8 | 0x00 << 16 | 0x00 << 24;
                Log.d(TAG, "durationPerPics is: " + durationPerPic);
                offset += 2;
                int repeat = (receivedMsg[offset] & 0xFF) | 0x00 << 8 | 0x00 << 16 | 0x00 << 24;
                offset += 1;
                usage += (receivedMsg[offset] & 0xFF) | 0x00 << 8 | 0x00 << 16 | 0x00 << 24;
                offset += 1;
                // jump the id field FOR NOW
                offset += 4;

                xhlPrograms.add(new XHLPrograms(programNameStr, i, numOfPics, false, false));
            }

            if (mConnectedDeviceName != null) {
                //deviceProgramDict.put(mConnectedDeviceName, temp);
                if (programFragmentCommunicator != null) {
                    //fragmentCommunicator.passProgramToFragment(deviceProgramDict.get(
                    // mConnectedDeviceName));
                    programFragmentCommunicator.passProgramToFragment(xhlPrograms);
                    programFragmentCommunicator.passIntToFragment(getUsage());
                }
            }





        } catch (StringIndexOutOfBoundsException e) {
            Log.e(TAG, "StringIndexOutOfBoundsException");
            e.printStackTrace();

            requestProgramsBT(); // request retransmit
            // TODO show error
        }


        //for (int i = 0; i < receivedMsg.length; i++){
        //}
    }

    private void updatePlayingProgram(){
        if(xhlPrograms != null && programsToBePlayed != null){
            for(int i=0;i<xhlPrograms.size();i++){
                XHLPrograms temp1 = xhlPrograms.get(i);
                temp1.setIsPlaying(false);
                xhlPrograms.set(i,temp1);
            }
            for(int i=0;i<programsToBePlayed.size();i++){
                XHLPrograms temp2 = xhlPrograms.get(programsToBePlayed.get(i));
                temp2.setIsPlaying(true);
                xhlPrograms.set(programsToBePlayed.get(i),temp2);
            }

            if (mConnectedDeviceName != null) {
                if (programFragmentCommunicator != null) {
                    programFragmentCommunicator.passProgramToFragment(xhlPrograms);
                    //programFragmentCommunicator.passIntToFragment(getUsage());
                }
            }

        }else{
            Log.e(TAG, "Either xhlPrograms is null or programsToBePlayed is null");
        }
    }



    public void sendHandShakeDataBT() {
        if (mBTService.getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);

        //String message = "#XHL0" + hour + minute + second + "#XHL0\r\n";
        //byte[] send = message.getBytes();


        String headStr = "#XHLB0";
        byte[] head = headStr.getBytes();
        mBTService.write(head);

        byte[] date = new byte[3];
        date[0] = (byte) (hour & 0xFF);
        date[1] = (byte) (minute & 0xFF);
        date[2] = (byte) (second & 0xFF);
        mBTService.write(date);

        //String decoded = new String(date);
        //Log.e(TAG, "Sending date is "+decoded);

        String tailStr = "#XHLE0\r\n";
        byte[] tail = tailStr.getBytes();
        mBTService.write(tail);

    }

    public void requestProgramsBT() {
        if (mBTService.getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.can_not_load_existing_program, Toast.LENGTH_SHORT).show();
            return;
        }
        Log.d(TAG, "requestProgramBT");
        String requestStr = "#XHLB4#XHLE4\r\n";
        byte[] request = requestStr.getBytes();
        mBTService.write(request);

        sendProgramUpdateCountDown = new CountDownTimer(5000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Toast.makeText(getApplicationContext(),
                        R.string.request_program_failed,Toast.LENGTH_LONG).show();
            }
        }.start();

    }

    public void sendProgram(int whichProgram){
        if (mBTService.getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.can_not_proceed, Toast.LENGTH_SHORT).show();
            return;
        }
        Log.d(TAG, "sendProgramToDevice");

        String headStr = "#XHLB2program1";
        byte[] head = headStr.getBytes();
        mBTService.write(head);

        //byte[] data = { 0x00, 0x01, 0x01, 0x00, 0x01, (byte)0xFF, (byte)0xFF, (byte)0xFF,
        // (byte)0xFF };
        byte[] data = new byte[10];



        data[0] = ((byte) 0x00); //x
        data[1] = ((byte) 0x03); //x
        data[2] = ((byte) 0x03); //m
        data[3] = ((byte) 0xE8); //m
        data[4] = ((byte) 0x03); //n
        data[5] =  ((byte) 0x01); //s


        data[6] = ((byte) 0xFF);
        data[7] = ((byte) 0xFF);
        data[8] = ((byte) 0xFF);
        data[9] =  ((byte) 0xFF);


        mBTService.write(data);

        String tailStr = "#XHLE2\r\n";
        byte[] tail = tailStr.getBytes();
        mBTService.write(tail);

        if (pictures == null){
            pictures = new LinkedBlockingQueue<byte[]>();
        }else{
            pictures.clear();
        }

        //load dzk files
        if (whichProgram==1) {
            pictures.add(loadDZKFile("1.dzk",1));
            pictures.add(loadDZKFile("2.dzk",2));
            pictures.add(loadDZKFile("3.dzk",3));

        }else if (whichProgram == 2){
            pictures.add(loadDZKFile("4.dzk",1));
            pictures.add(loadDZKFile("5.dzk",2));
            pictures.add(loadDZKFile("6.dzk",3));
        }else if (whichProgram == 3){
            pictures.add(loadDZKFile("7.dzk",1));
            pictures.add(loadDZKFile("8.dzk",2));
            pictures.add(loadDZKFile("9.dzk",3));
        }

        sendPicCountDown = new CountDownTimer(10000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Toast.makeText(getApplicationContext(),
                        R.string.send_picture_failed,Toast.LENGTH_LONG).show();
                pictures.clear();
            }
        }.start();


    }

    private byte[] loadDZKFile(String filename, int sequenceNum){
        byte[] result = null;
        byte[] data = new byte[2];
        data[0] = (byte) ((sequenceNum >> 8) & 0xFF) ;
        data[1] = (byte) (sequenceNum & 0xFF);

        AssetManager am = getApplicationContext().getAssets();
        try {
            InputStream is = am.open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String text = new String(buffer);
            Log.v(TAG, text);

            result = new byte[data.length + buffer.length];
            System.arraycopy(data,0, result,0,data.length);
            System.arraycopy(buffer,0,result,data.length,buffer.length);


        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;

    }


    public void deleteSelectedPrograms(ArrayList<Integer> selectedPrograms){
        if (mBTService.getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(getApplicationContext(), R.string.can_not_proceed, Toast.LENGTH_SHORT).show();
            Log.e(TAG, "Bluetooth device is not connected, abort");
            return;
        }
        Log.d(TAG, "deleteSelectedPrograms");
        Log.d(TAG,"selectedPrograms.size() = " + selectedPrograms.size());

        String requestStrBegin = "#XHLB8";
        byte[] requestBegin = requestStrBegin.getBytes();
        mBTService.write(requestBegin);

        byte[] programId = new byte[selectedPrograms.size()];

        for (int i=0;i<selectedPrograms.size();i++){
            Log.d(TAG,"selectedPrograms.get(i)="+selectedPrograms.get(i));
            programId[i] = (byte) (selectedPrograms.get(i) & 0xFF);
        }
        mBTService.write(programId);


        String requestStrEnd = "#XHLE8\r\n";
        byte[] requestEnd = requestStrEnd.getBytes();
        mBTService.write(requestEnd);

    }

    public void playSelectedPrograms(ArrayList<Integer> selectedPrograms){
        if (mBTService.getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(getApplicationContext(), R.string.can_not_proceed, Toast.LENGTH_SHORT).show();
            Log.e(TAG, "Bluetooth device is not connected, abort");
            return;
        }
        Log.d(TAG, "playSelectedPrograms");
        Log.d(TAG,"selectedPrograms.size() = " + selectedPrograms.size());

        programsToBePlayed = selectedPrograms;

        String requestStrBegin = "#XHLB1";
        byte[] requestBegin = requestStrBegin.getBytes();
        mBTService.write(requestBegin);

        byte[] programId = new byte[selectedPrograms.size()+1];
        programId[0] = (byte) (selectedPrograms.size() & 0xFF);

        for (int i=0;i<selectedPrograms.size();i++){
            Log.d(TAG,"selectedPrograms.get(i)="+selectedPrograms.get(i));
            programId[i+1] = (byte) (selectedPrograms.get(i) & 0xFF);
        }
        mBTService.write(programId);


        String requestStrEnd = "#XHLE1\r\n";
        byte[] requestEnd = requestStrEnd.getBytes();
        mBTService.write(requestEnd);
    }

    /*
        This function is executed immidiately after the time has been sync-ed
     */
    public void sendBikeConfig(int wheelSize, boolean meterOn){
        if (mBTService.getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.can_not_proceed, Toast.LENGTH_SHORT).show();
            Log.e(TAG, "Bluetooth device is not connected, abort");
            return;
        }

        String headerStr = "#XHLB3";
        byte[] header =headerStr.getBytes();
        mBTService.write(header);

        byte[] data = new byte[2];
        data[0] = (byte) (wheelSize & 0xFF);
        if (meterOn == true) {
            data[1] = 0x01;
        }else{
            data[1] = 0x00;
        }
        mBTService.write(data);

        String tailStr = "#XHLE3\r\n";
        byte[] tail = tailStr.getBytes();
        mBTService.write(tail);


        sendConfigCountDown = new CountDownTimer(5000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                /*
                Toast.makeText(getApplicationContext(),
                        R.string.send_config_failed,Toast.LENGTH_LONG).show();
                        */
            }
        }.start();


    }



    /*
       * This class deal with incoming message from bluetooth device by processing the byte array of
       * the message on the queue and remove the element from the queue.
     */
    public class InterpretBTMessageThread extends Thread{
        private boolean terminateThread;
        private byte[] receivedMsg;
        private int counter;
        private Handler msgHandler;
        //private int messageType;

        private byte messageType;


        public InterpretBTMessageThread(Handler handler){
            msgHandler = handler;
        }

        public void run(){
            while(true){
                try {

                    if (receivedMsgQueue.size() > 0) {
                        Log.e(TAG, "Current size of the queue is " + receivedMsgQueue.size());
                        // get an element from the queue
                        PairElement pairElement = receivedMsgQueue.take();
                        byte[] msg = pairElement.getMsg();
                        int validLen = pairElement.getValidLen();
                        Log.e(TAG,"The size of the current element: " + validLen);

                        /*
                        for (int i=0;i<validLen;i++){
                            int temp = (msg[i] & 0xFF) | 0x00 << 8 | 0x00 << 16 | 0x00 << 24;
                            Log.d(TAG,"data = "+temp);
                        }
                        */


                        for(int i=0;i<validLen;i++){
                            //if #XHLB is detected
                            if(msg[i]== (byte) 0x23 && msg[i+1]==(byte) 0x58 &&
                                    msg[i+2]==(byte) 0x48 && msg[i+3]==(byte) 0x4C &&
                                    msg[i+4]==(byte)0x42){
                                Log.e(TAG,"#XHLB is detected");
                                // reset receivedMsg
                                receivedMsg = new byte[1024];
                                counter = 0;
                                messageType = msg[i+5];
                                receivedMsg[counter] = msg[i];
                            }
                            //if #XHLE is detected
                            else if(msg[i]== (byte) 0x23 && msg[i+1]==(byte) 0x58 &&
                                    msg[i+2]==(byte) 0x48 && msg[i+3]==(byte) 0x4C &&
                                    msg[i+4]==(byte)0x45){
                                Log.e(TAG,"#XHLE is detected");
                                // enters only when the message type ending matches
                                // the message type beginning
                                if(msg[i+5] == messageType) {
                                    Log.d(TAG,"XHLB matches XHLE");
                                    receivedMsg[counter] = msg[i];
                                    receivedMsg[counter + 1] = msg[i + 1];
                                    receivedMsg[counter + 2] = msg[i + 2];
                                    receivedMsg[counter + 3] = msg[i + 3];
                                    receivedMsg[counter + 4] = msg[i + 4];
                                    receivedMsg[counter + 5] = msg[i + 5];

                                    if(msg[i+5] == 0x30){
                                        //if the response is OK
                                        if(receivedMsg[6]==0x4F && receivedMsg[7]==0x4B) {
                                            msgHandler.obtainMessage(MainActivity.SYNC_CONFIG,
                                                    -1, -1, receivedMsg).sendToTarget();
                                        }
                                    }else if(msg[i+5] == 0x31){ // acknowledgement on received programs to play
                                        msgHandler.obtainMessage(MainActivity.PROGRAM_TO_PLAY_UPDATED,
                                                -1, -1, null).sendToTarget();
                                    }else if (msg[i+5] == 0x34) { // program sync
                                        Log.e(TAG, "sending receivedMsg type 0x34 " +
                                                "to Target...LIST_PROGRAMS");
                                        msgHandler.obtainMessage(MainActivity.LIST_PROGRAMS,
                                                -1, -1, receivedMsg).sendToTarget();


                                    } else if (msg[i+5] == 0x38){ // acknowledgement on received deleted program
                                        Log.e(TAG,"sending receivedMsg type 0x38 to Target..." +
                                                "REFRESH_DISPLAYED_PROGRAM");
                                        //if the response is OK
                                        if(receivedMsg[6]==0x4F && receivedMsg[7]==0x4B) {
                                            msgHandler.obtainMessage(MainActivity.REQUEST_PROGRAMS,
                                                    -1, -1, receivedMsg).sendToTarget();
                                        }
                                    } else if(msg[i+5] == 0x32){ // acknowledgement on received request to add program
                                        Log.e(TAG,"sending receivedMsg type 0x32 to Target..." +
                                                "SEND_NEXT_PICTURE");
                                        msgHandler.obtainMessage(MainActivity.SEND_NEXT_PICTURE,
                                                -1, -1, receivedMsg).sendToTarget();
                                    } else if(msg[i+5] == 0x39){ // acknowledgement on received picture
                                        if(receivedMsg[6]==0x4F && receivedMsg[7]==0x4B){
                                            Log.e(TAG,"sending receivedMsg type 0x39 to Target..." +
                                                    "SEND_NEXT_PICTURE");
                                            msgHandler.obtainMessage(MainActivity.SEND_NEXT_PICTURE,
                                                    -1, -1, receivedMsg).sendToTarget();
                                        }
                                        else if(receivedMsg[6]==0x46 && receivedMsg[7]==0x41 && receivedMsg[8]==0x49 && receivedMsg[9]==0x4C){
                                            msgHandler.obtainMessage(MainActivity.FAIL_TO_RECEIVE,-1,-1,receivedMsg).sendToTarget();


                                        }


                                    } else if(msg[i+5] == 0x33){
                                        if(receivedMsg[6]==0x4F && receivedMsg[7]==0x4B){
                                            msgHandler.obtainMessage(MainActivity.CONFIG_UPDATED,-1,-1,receivedMsg).sendToTarget();
                                        }

                                    }
                                    else if(msg[i+5] == 0x36){ //测速和路程
                                        msgHandler.obtainMessage(MainActivity.MILAGE_UPDATE,-1,-1,receivedMsg).sendToTarget();

                                    }
                                    receivedMsg = null;
                                }
                            } else{
                                if (receivedMsg != null) {
                                    receivedMsg[counter] = msg[i];
                                }
                            }

                            if(receivedMsg != null) {
                                counter++;
                                counter = counter % 1024;

                            }
                        }

                    }

                    if (terminateThread == true) {
                        Log.e(TAG, "InterpretBTMessageThread terminated");
                        break;
                    }
                }catch(InterruptedException e){
                    e.printStackTrace();
                }catch(NullPointerException e){
                    e.printStackTrace();
                }
            }
            return;
        }

        public void terminate(){
            terminateThread = true;
        }
    }

    /*
     * This class is created to add a pair of values as one element to the queue (the message as
     * byte array and the valid values in the message), since the byte array is allocated as
     * a fixed length but it does not necessarily gets filled up each time a BT message is received.
     */
    private class PairElement{
        private byte[] msg;
        private int validLen;

        private PairElement(byte[] msg, int validLen){
            this.msg = msg;
            this.validLen = validLen;
        }

        private byte[] getMsg(){ return msg; }

        private int getValidLen(){ return validLen; }

    }

    public class BTDevice{
        private String deviceName;
        private String deviceAddress;
        private int wheelSize;
        private boolean milageOn;

        public BTDevice(String name, String address, int wheelSize, boolean milageOn){
            deviceName = name;
            deviceAddress = address;
            this.wheelSize = wheelSize;
            this.milageOn = milageOn;
        }

        public String getDeviceName(){ return deviceName; }

        public String getDeviceAddress(){ return deviceAddress; }

        public int getWheelSize(){ return wheelSize; }

        public boolean isMilageOn(){ return milageOn; }

        public void setWheelSize(int wheelSize){
            this.wheelSize = wheelSize;
        }

        public void setMilageOn(boolean milageOn){
            this.milageOn = milageOn;
        }
    }


}
