package yc.android.fenghuolun.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.Toast;

import yc.android.fenghuolun.R;
import yc.android.fenghuolun.constant.Constant;

/**
 * Created by yichangzhang on 15/08/2014.
 */
public class LoadingActivity extends FragmentActivity {

    private long exitTime;
    private String press_again_to_exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_loading);

        press_again_to_exit = getResources().
                getString(R.string.press_again_to_exit);

        // displaying loading image for a few seconds
        new Handler().postDelayed(new Runnable(){
            public void run(){
                //wait then kill the current activity

                SharedPreferences prefs = getSharedPreferences(
                        Constant.USER_DETAIL, 0);
                String username = prefs.getString(
                        Constant.user_field, "");
                String token = prefs.getString(
                        Constant.token_field, "");

                if (!username.equals("") && !token.equals("")){
                    //TODO check login detail
                    finish();
                    Intent i = new Intent(LoadingActivity.this, MainActivity.class);
                    startActivity(i);
                }
                else{
                    finish();
                    Intent i = new Intent(LoadingActivity.this, LoginActivity.class);
                    startActivity(i);
                }
            }
        }, 2000);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN)
        {

            //System.currentTimeMillis()无论何时调用，肯定大于2000
            if((System.currentTimeMillis()-exitTime) > 2000)
            {
                Toast.makeText(getApplicationContext(), press_again_to_exit,
                        Toast.LENGTH_SHORT).show();
                exitTime = System.currentTimeMillis();
            }
            else
            {
                finish();
                //System.exit(0);
            }

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
