package yc.android.fenghuolun.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import yc.android.fenghuolun.R;

/**
 * Created by zhaopei on 2/10/2014.
 */
public class ModifyPersonalInfoActivity extends Activity {

    private static final String host_url = "http://192.168.1.103:8000";
    private static final String get_personalInfo_url = host_url + "/users/me/";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.modify_personal_information);
        Context ct = getApplication();

        final EditText ln_edit = (EditText) findViewById(R.id.mine_modify_personal_info_ln_edit);
        final EditText fn_edit = (EditText) findViewById(R.id.mine_modify_personal_info_fn_edit);
        final EditText phone_edit = (EditText) findViewById(R.id.mine_modify_phone_edit);
        final EditText address_edit = (EditText) findViewById(R.id.mine_modify_address_edit);

        displayInfo(ln_edit, fn_edit, phone_edit, address_edit);


        Button btnModifyPersonalInfoConfirm = (Button) findViewById(R.id.btn_mine_modify_personal_info_confirm);
        btnModifyPersonalInfoConfirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //TODO check if login detail is valid
//                String new_ln_str = new_ln_edit.getText().toString();
//                String new_fn_str = new_fn_edit.getText().toString();
//                String new_phone_str = new_phone_edit.getText().toString();
//                String new_address_str = new_address_edit.getText().toString();

                finish();
                //Toast.makeText(v.getContext(), "button login is clicked", Toast.LENGTH_SHORT).show();
            }
        });

        Button btnModifyPersonalInfoCancel = (Button) findViewById (R.id.btn_mine_modify_personal_info_cancel);
        btnModifyPersonalInfoCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //TODO check if login detail is valid

                finish();
                //Toast.makeText(v.getContext(), "button login is clicked", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void displayInfo(final EditText ln_edit, final EditText fn_edit, final EditText phone_edit, final EditText address_edit) {
        String tk = "679370a75649dfd0ea81dd4318641bd48d24a954"; // Later need to be grabbed from sharepereferance
//            SharedPreferences prefs = rootView.getContext().getSharedPreferences(
//                    PERSONAL_CREDENTIAL, 0);
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Authorization", "Token " + tk);




        client.get(get_personalInfo_url, new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                  JSONObject errorResponse) {
                //TODO handle different errors

                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {


                String ln_str = null;
                String fn_str = null;
                String phone_str = null;
                String address_str = null;
//                    String ava_device_pic TBD
                int ava_devices_num = 0;

                try {
                    ln_str = response.getString("last_name");
                    fn_str = response.getString("first_name");
                    phone_str = response.getString("phone");
                    address_str = response.getString("address");
                    ava_devices_num = response.getJSONArray("devices").length();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                ln_edit.setText(ln_str);
                fn_edit.setText(fn_str);
                phone_edit.setText(phone_str);
                address_edit.setText(address_str);
//                    personal_info_avai_pic.setText(ava_pic_str);

                super.onSuccess(statusCode, headers, response);
            }
        });
    }


}
