package yc.android.fenghuolun.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import yc.android.fenghuolun.R;

/**
 * Created by yichangzhang on 14/10/2014.
 */
public class AddDeviceActivity extends Activity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_add_device);

        Intent i = getIntent();

        TextView txt_bt_id = (TextView)findViewById(R.id.bt_id);
        txt_bt_id.setText(i.getStringExtra("bt_id"));

        TextView txt_device_id = (TextView)findViewById(R.id.device_id);
        txt_device_id.setText(i.getStringExtra("device_id"));

        TextView txt_active_code = (TextView)findViewById(R.id.active_code);
        txt_active_code.setText(i.getStringExtra("active_code"));

        Button confirm = (Button) findViewById (R.id.add_device_confirm);
        confirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Button cancel = (Button) findViewById (R.id.add_device_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
